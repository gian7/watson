<?php
/**
 * The template part for displaying single posts
 *
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="article-content">
		<?php
			the_content();

		?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
