<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package mlife-theme
 */

get_header();

//checking if the backup image should be done by tags or by the category (tags for most BU, categories for MRO)
global $backup_taxonomy_select;
$backup_taxonomy_select = "post_tag";
if(get_theme_mod('backup_taxonomy_select')){
	$backup_taxonomy_select = get_theme_mod('backup_taxonomy_select');
}

// creating an array of the images given for each tag or category or the product itself to backup out of stock products
global $random_backup_img_url;
$random_backup_img_url = array();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();
		
		
			?>
			
			<?php $image = get_field('header_image');  
				
					if($image){ // if an header is set
					?>
					
					<div class="hero" style="<?php  echo "background-image: url('".$image['url']."');";  ?>">  
					<?php 	
						}else{ ?>
							<div class="hero no-image" >  
					
					<?php  }?>

						<h1 itemprop="name headline"><?php the_title(); ?></h1> 
						<div class="whitebox_info">
							<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
							    <?php if(function_exists('bcn_display')) // calling the breadcrumb
							    {
							        bcn_display();
							    }?>
							</div>
							<?php  
								// Previous/next post navigation.
								the_post_navigation( array(
									'next_text' => '<span class="meta-nav" aria-hidden="true">'._x( 'Next', 'mlife-theme' ).'</span><i class="fa fa-angle-right"></i>',
									'prev_text' => '<i class="fa fa-angle-left"></i><span class="meta-nav" aria-hidden="true">'._x( 'Previous', 'mlife-theme' ).'</span>',
								) );
							?>
							<div class="printer"><?php  the_post_thumbnail('thumbnail');?></div>
							<div class="clear"></div>
						</div>
						<?php 
							$cat = new WPSEO_Primary_Term('category', get_the_ID()); // looking for the primary category
							$cat = $cat->get_primary_term();
							if($cat){
								$tab_cat = get_category($cat); 
								
							}else{
								$categories = get_the_category();
								$tab_cat = $categories[0]; 
							}
							echo '<div class="category_post '.$tab_cat->slug.' '.mlife_theme_term_customisation($tab_cat->term_id).'"><a href="'.get_category_link($tab_cat->term_id).'"><span>'.$tab_cat->name.'</span></a></div>';
							
							//if the backup taxonomy is category (change in customizer -> Random Backup)
							if($backup_taxonomy_select=="category"){
								$random_backup_img_url = array_merge ($random_backup_img_url, mlife_theme_get_backup_images('term_' . $term->term_id));
							}
						?>
					</div>
					
				<?php
					
					
					
					$link = urlencode(hybris_url(get_permalink()));
					$title = urlencode(get_the_title());
					$date = get_the_date("d.m.Y");
					?>
					<div class="more-info">
						<div class="tag_text"><?php echo mlife_theme_get_the_tag_list();?></div>
						<div class="posted-on"><?php echo $date ?></div>
						<div class="social_sharing">
							<ul>
								<li><?php echo _x( 'Share this Article:', 'mlife-theme' ); ?></li>
								<li><a href="https://twitter.com/share?url=<?php echo $link; ?>&text=<?php echo $title; ?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
								<li><a href="http://www.facebook.com/sharer.php?u=<?php echo $link; ?>&t=<?php echo $title; ?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="https://plus.google.com/share?url=<?php echo $link; ?>" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
								<li><a href="https://pinterest.com/pin/create/button/?url=<?php echo $link; ?>" target="_blank"><i class="fab fa-pinterest"></i></a></li>
								
							</ul>
						</div>
						<div class="clear"></div>
					</div>

			<?php
			
			
			// there is not backup images from the tag function mlife_theme_get_the_tag_list() get the ones from the theme
			if(count($random_backup_img_url)<1){
				if(get_theme_mod( 'random1' )!=""){
					$random_backup_img_url[] = esc_url(get_theme_mod( 'random1' ));
				}
				if(get_theme_mod( 'random2' )!=""){
					$random_backup_img_url[] = esc_url(get_theme_mod( 'random2' ));
				}
				if(get_theme_mod( 'random3' )!=""){
					$random_backup_img_url[] = esc_url(get_theme_mod( 'random3' ));
				}
				if(count($random_backup_img_url)<1){ // if there is no back up from the theme put a place holder
					if (buIsMCH()) {
                        $random_backup_img_url[] = get_template_directory_uri() . '/images/backup-mch.jpg';
                    } else {
                        $random_backup_img_url[] = get_template_directory_uri() . '/images/backup.jpg';
                    }
				}
			}
			
			
			// Include the single post content template.
			
			get_template_part( 'template-parts/content', 'single' );

					?>
					
					<div class="more-info footer">
						<div class="tag_text"><?php echo mlife_theme_get_the_tag_list();?></div>
						<div class="posted-on"><?php echo $date ?></div>
						<div class="social_sharing">
							<ul>
								<li><?php echo _x( 'Share this Article:', 'mlife-theme' ); ?></li>
								<li><a href="https://twitter.com/share?url=<?php echo $link; ?>&text=<?php echo $title; ?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
								<li><a href="http://www.facebook.com/sharer.php?u=<?php echo $link; ?>&t=<?php echo $title; ?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="https://plus.google.com/share?url=<?php echo $link; ?>" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
								<li><a href="https://pinterest.com/pin/create/button/?url=<?php echo $link; ?>" target="_blank"><i class="fab fa-pinterest"></i></a></li>
							</ul>
						</div>
						<div class="clear"></div>
					</div>
				
			<?php 	
				if(get_field('background_image')){ // if a background image is set, leave it here for javascript in footer
					$image_background = get_field('background_image');
					
					echo '
						<style>
							body.custom-background {
								background-image: url("'.$image_background['url'].'") !important;
							}
						</style>
						<div id="background-single" style="display:none" data-src="'.$image_background['url'].'"></div>
					';
				}
				
				
		

				/*
				 *if the content bottom mlife is not empty show it / edit in BO Appearance->Widgets / Normally contains the menu with home icon not shown on home page
				*/
				if ( is_active_sidebar( 'content_bottom_mlife' ) ) : ?>
				<aside id="content_bottom_mlife" class="content_bottom_mlife" role="complementary">
					<?php dynamic_sidebar( 'content_bottom_mlife' ); ?>
				</aside>
			    <?php endif;
	
	// End of the loop.
		endwhile;
		?>


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
