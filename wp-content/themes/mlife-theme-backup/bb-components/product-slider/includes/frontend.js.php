jQuery(document).ready(function () {
    jQuery('#product-slider-<?php echo $id; ?>').slick({
        dots: false,
        infinite: false,
        speed: 300,
        arrows: true,
        prevArrow: ".slick-prev-arrow-<?php echo $id; ?>",
        nextArrow: ".slick-next-arrow-<?php echo $id; ?>",
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 450,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 320,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false
                }
            }
        ]
    });

// On swipe event
// @TODO make logic when product tile is swipe updated the arrows
    jQuery('#product-slider-<?php echo $id; ?>').on('swipe', function (event, slick, direction) {
        console.log('swipe  was hit');
        console.log(event);
        console.log(slick);
        console.log(direction);
    });

// On edge hit
    // @TODO make logic when breakpoint is hit updating the pagination.
    jQuery('#product-slider-<?php echo $id; ?>').on('breakpoint', function (event, slick, breakpoint) {
        console.log('breakpoint was hit')
        console.log(event);
        console.log(slick);
        console.log(breakpoint);
    });

});