<div class="featured-product <?php echo $settings->select_layout;?> <?php echo $settings->has_border;?>" style="background-image: url('<?php echo wp_get_attachment_url($settings->bg_field); ?>')">
	<div class="overall-container">
		
		<div class="product-tile">
			<div class="backup">
				<?php
					global $random_backup_img_url;
					$alt = "";
					$url = "";
					$label = "";
					$img = "";
					
					
					// if there is no caterogy chosen on the product tile
					if($settings->select_hybris_category=='none'){
						$alt = get_the_title();
						
						$img = $random_backup_img_url[array_rand ( $random_backup_img_url )]; // get a random image from the table build either with the tags associated to the article or the theme's back up or worst case scenario a place holder image
					}else{
						
						$category_backup = array(); // create an an array
						
						// get the backup images
						$category_backup = mlife_theme_get_backup_images($settings->select_hybris_category);
						
						
						$url = get_field('url',$settings->select_hybris_category );
						$alt = get_field('label',$settings->select_hybris_category );
						$label = $alt;	
	
						
						$img = $category_backup[array_rand ( $category_backup )]; // get random images
						 
					}
					
					// build the back up
					if($url!=""){ // Checking it the url is setup otherwise it means the image shouldn't be clickable
						echo '<a href="'.$url.'" title="'.$alt.'">';
					}
					
					echo '<img src="'.$img.'" alt="'.$alt.'" />';
					if($label!=""){
						echo '<span class="label"><span>'.$label.'</span></span>';
					}
					if($url!=""){// Checking it the url is setup otherwise it means the image shouldn't be clickable
						echo '</a>';
					}
					
					
				?>
			</div>
			<div class="instock">
				<div class="product-image <?php if(wp_get_attachment_url($settings->product_image)){ echo 'no-dummy'; } ?>">
					<a href="<?php echo HYBRIS_URL."/p/".$settings->product_id;?>" class="product-image-link">
						
						<?php 
						if(wp_get_attachment_url($settings->product_image)){
							echo '<img src="'.wp_get_attachment_url($settings->product_image).'" alt="'.$settings->product_id.'" />';
						}else{ ?>
							<img src="{{product(<?php echo $settings->product_id;?>):primaryImageUrl}}" alt="<?php echo $settings->product_id;?>" />
						<?php  }
						?>
					</a>
				</div>
				<div class="info">
					<span class="product-brand">
						<span data-hybris-product-data="product(<?php echo $settings->product_id;?>):brandData.name">
							Brand
						</span>
					</span>
					<span class="product-name">
						<span data-hybris-product-data="product(<?php echo $settings->product_id;?>):rangeName">
							Product Name
						</span>
					</span>
					<span class="product-desc">
						<span data-hybris-product-data="product(<?php echo $settings->product_id;?>):name">
							Description of the Product
						</span>
					</span>
					<span class="product-price">
						<span class="product-price-old">
							<span data-hybris-product-data="product(<?php echo $settings->product_id;?>):baseOptions[0].selected.priceData.formattedValue">
								Price
							</span>
						</span>
						<span class="product-price-new">
							<span data-hybris-product-data="product(<?php echo $settings->product_id;?>):baseOptions[0].selected.igcMarkDownPrice.formattedValue">
								null
							</span>
						</span>
						<?php 
						/*if($settings->show_size=="show"){
						
						 echo '
						<span class="product-size">
							- <span data-hybris-product-data="product(<?php echo $settings->product_id;?>):baseOptions[0].selected.igcVariantOptionQualifiers.variantSize">
								variantSize
							</span>
						</span>'; }*/
						?>
					</span>
					
					<!-- DESKTOP BUTTON -->
						<a  href="/p/<?php echo $settings->product_id;?>/quickView" class="product-link quickview desktop fancybox.ajax" target="_blank" onclick="_gaq.push(['_trackEvent','CampaignPage','ShopNow','<?php echo $settings->product_id;?>']);" ><span><?php echo esc_attr_x( 'Shop Now', 'mlife-theme' ); ?></span></a>
					<!-- DESKTOP MOBILE BUTTON -->
					<!-- MOBILE BUTTON -->
						<a id="addToCartButton_<?php echo $settings->product_id;?>" class="product-link mobile" type="button" onclick="openPopoverToChooseVariant('/p/<?php echo $settings->product_id;?>'); _gaq.push(['_trackEvent','CampaignPage','ShopNow','<?php echo $settings->product_id;?>']);" value="SHOP NOW"><span><?php echo esc_attr_x( 'Shop Now', 'mlife-theme' ); ?></span></a>
					<!-- END MOBILE BUTTON -->
					
				</div>
			</div>
		</div>
		<?php
			if($settings->select_layout=="layout-1coltext"||$settings->select_layout=="layout-2coltext"){
				?>
				
				<div class="text">
				<?php
					if($settings->title!=""){
				?>
					<h2><?php echo $settings->title;?></h2>
				<?php
					}
					if($settings->text_editor!=""){
						echo $settings->text_editor;
					}
				?>
				</div>
				<?php
			}
			
		?>
	</div>
</div>