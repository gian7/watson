<?php
$skues = [];
foreach ($settings->products as $product) {
    array_push($skues,$product->product_id);
}
?>
<div class="multiAddToBasket" data-skews="<?php echo implode(',', $skues);?>"
     data-textfailure="<?php echo  $settings->text_failure; ?>" data-textsuccess="<?php echo  $settings->text_success; ?>">
    <button class="trigger <?php echo  $settings->class; ?>" type="button"><?php echo  $settings->text_cta; ?></button>
    <div class="multiAddToBasketResponse">
        <div class="matbFailure">
        </div>
        <div class="matbSuccess">
        </div>
        <div class="matbLoading">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
        </div>
    </div>
    <div class="multiAddToBasketPopup">
        <?php echo  $settings->text_success; ?>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="/elab/eLab/2018LEAB2028/css/addToBasket.css">
<script src="/elab/eLab/2018LEAB2028/js/addToBasket.js"></script>