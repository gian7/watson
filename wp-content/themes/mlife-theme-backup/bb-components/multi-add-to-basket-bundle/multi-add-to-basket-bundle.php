<?php

class MLifeBBMultiAddToBasketBundleClass extends FLBuilderModule
{

    public function __construct()
    {
        parent::__construct(array(
            'name' => __('Multi add to basket bundle', 'fl-builder'),
            'description' => __('Add multiple product to basket at once', 'fl-builder'),
            'category' => __('Hybris MLife', 'fl-builder'),
            'dir' => MLIFE_BB_DIR . 'product-slider/',
            'url' => MLIFE_BB_URL . 'product-slider/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled' => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
            'icon' => 'button.svg'
        ));

        wp_enqueue_script('slick-script');
        wp_enqueue_script('slick-init-script');
        wp_enqueue_style('slick-style');
        wp_enqueue_style('slick-style-theme');
    }

}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('MLifeBBMultiAddToBasketBundleClass', array(
    'products' => array(
        'title' => __('Products', 'fl-builder'),
        'sections' => array(
            'general' => array(
                'title' => '',
                'fields' => array(
                    'text_cta'     => array(
                        'type'          => 'text',
                        'label'         => __('CTA text', 'fl-builder'),
                        'default'       => '',
                        'maxlength'     => '',
                        'size'          => '50',
                        'placeholder'   => 'example: ADD ALL TO BASKET',
                        'class'         => '',
                        'description'   => '',
                        'help'          => 'Text of the button or CTA that user reads',
                    ),
                    'text_success'     => array(
                        'type'          => 'text',
                        'label'         => __('Text in case of success', 'fl-builder'),
                        'default'       => '',
                        'maxlength'     => '',
                        'size'          => '50',
                        'placeholder'   => 'example: Added to Basket!',
                        'class'         => '',
                        'description'   => '',
                        'help'          => 'Feedback to user when everything went OK',
                    ),
                    'text_failure'     => array(
                        'type'          => 'text',
                        'label'         => __('Text in case of failure', 'fl-builder'),
                        'default'       => '',
                        'maxlength'     => '',
                        'size'          => '50',
                        'placeholder'   => 'example: It looks like something went wrong',
                        'class'         => '',
                        'description'   => '',
                        'help'          => 'Errors could happen, add feeback text in case something goes wrong.',
                    ),
                    'products' => array(
                        'type' => 'form',
                        'label' => __('Product', 'fl-builder'),
                        'form' => 'product_id_form', // ID from registered form below
                        'preview_text' => 'product', // Name of a field to use for the preview text
                        'multiple' => true,
                        'minimum'
                    ),
                ),
            ),
        ),
    ),
));
/**
 * Register a settings form to use in the "form" field type above.
 */
FLBuilder::register_settings_form('product_id_form', array(
    'title' => __('Add Product', 'fl-builder'),
    'tabs' => array(
        'general' => array( // Tab
            'title' => __('General', 'fl-builder'), // Tab title
            'sections' => array( // Tab Sections
                'general' => array( // Section
                    'title'         => __('Product', 'fl-builder'), // Section Title
                    'fields'        => array( // Section Fields
                        'product_id'     => array(
                            'type'          => 'text',
                            'label'         => __('SKU', 'fl-builder'),
                            'default'       => '',
                            'maxlength'     => '',
                            'size'          => '50',
                            'placeholder'   => '47459210',
                            'class'         => '',
                            'description'   => '',
                            'help'          => 'This is the product ID/SKU',
                        )
                    ),
                ),
            ),
        ),
    ),
));