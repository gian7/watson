h2.content-heading.mobile-product-carousel,
h2.content-heading,
.content-heading>span,
.content-heading {
    background: none !important;
    font-size: 30px !important;
	color: #2d2d2d !important;
	font-weight: 200 !important;
	font-family: Raleway, sans-serif !important;
}
.article-content div.container-fluid.product-grid ul.row-fluid.item-list{
	padding: 0;
	margin: 0;
}
.article-content div.container-fluid.product-grid ul.row-fluid.item-list li,
.article-content .carousel-product-holder ul li{
	background: none;
	padding: 0 ;
	text-align: center  !important;
}
.product-panel.pull-left {
    width: 100%;
}
.article-content ul.item-info .item-brand h2 ,
.article-content div.container-fluid.product-grid ul.row-fluid.item-list ul.item-info.plp-product-div-tobottom li.item-brand,
#categoria ul.productdesc-text li.brand-name {
    text-transform: uppercase;
	font-size : 17px !important;
	font-weight: 700;
	font-family: Times;
	padding: 0px;
	height: auto !important;
	color : #333 !important;
	line-height: normal !important;
}
.article-content ul.item-info .item-brand h2 a,
.article-content div.container-fluid.product-grid ul.row-fluid.item-list ul.item-info.plp-product-div-tobottom li.item-range a,
#categoria ul.productdesc-text li.brand-name a{
	color : #333 !important;
}
.article-content div.container-fluid.product-grid ul.row-fluid.item-list ul.item-info.plp-product-div-tobottom li.item-range,
#categoria ul.productdesc-text li.range-name {
    font-size : 15px  !important;
	font-weight: 400;
	font-family: Raleway;
	padding: 4px 0 3px  !important;
	height: auto !important;
	color : #333 !important;
}
#categoria ul.productdesc-text li.range-name a {
    font-size : 15px  !important;
	font-weight: 400;
	font-family: Raleway;
	height: auto !important;
	color : #333 !important;

}
.article-content div.container-fluid.product-grid ul.row-fluid.item-list ul.item-info.plp-product-div-tobottom li.item-name,
#categoria ul.productdesc-text li.product-name {
   	font-size : 13px !important;
	font-weight: 400;
	font-family: Raleway;
	padding: 1px 0px  !important;
	height: auto !important;
	min-height: 36px !important;
	color : #333 !important;
}
.article-content div.container-fluid.product-grid ul.row-fluid.item-list ul.item-info.plp-product-div-tobottom li.item-name{
	min-height: 51px !important;
}
.article-content div.container-fluid.product-grid ul.row-fluid.item-list ul.item-info.plp-product-div-tobottom li.item-name a,
#categoria ul.productdesc-text li.product-name a{
	color : #333 !important;
}
.article-content div.container-fluid.product-grid ul.row-fluid.item-list ul.item-info.plp-product-div-tobottom li.item-price,
#categoria ul.productdesc-text li.price {
   	font-size: 17px  !important;
    font-weight: 700;
    padding: 5px;
    height: auto !important;
    min-height: 42px !important;
    display: inline;
}


.article-content div.container-fluid.product-grid ul.row-fluid.item-list ul.item-info.plp-product-div-tobottom li.item-price,
#categoria ul.productdesc-text li.members-price {
    height: auto !important;

}
.article-content div.container-fluid.product-grid ul.row-fluid.item-list ul.item-info.plp-product-div-tobottom li.item-price,
#categoria ul.productdesc-text li.prod-unit-price {
	height: auto !important;
}

.article-content .jcarousel-container ul li.jcarousel-item {
	margin-right: 36px !important;
    width: 205px;
}
.article-content div.container-fluid.product-grid ul.row-fluid.item-list li.span6{
	width:23%;
	margin: 0 1%;
    padding: 0;
    float: left; 
    border-bottom: 0px solid #808080;
}
.product-panel > a {
    margin: 0 auto;
}
.article-content div.container-fluid.product-grid ul.row-fluid.item-list ul.item-info.plp-product-div-tobottom li.item-raty.clearfix.rating-listitem-li,
h2.content-heading.mobile-product-carousel hr,
.paginatore_btn{
	display: none;
}
.article-content .product-grid li:nth-child(2n+1) .product-container, 
.article-content .product-grid li:nth-child(2n) .product-container {
    padding-right: 0;
    padding-left: 0;
}
.article-content .product-grid li .product-container {
    width: 100%;
}
.article-content .product-grid .item-list>li:nth-child(3n+1) {
    clear: none;
}
.product-grid li .product-container,
.product-grid li:nth-child(3n+3) .product-container,
.product-grid li:nth-child(3n+1) .product-container {
    border: 0px;
}
.product-grid ul.item-info .item-price-member {
	min-height: inherit;
}

.product-grid .input-field input {
    background-color: #702785 !important;
	border-radius: 0px;
	-o-border-radius :0px;
	-ms-border-radius : 0px;
	-moz-border-radius :0px;
	-webkit-border-radius :0px;
}
.product-grid .input-field input:hover {
	background-color: #c894d7 !important;
}

.prodpanels-text .brand-name a, 
.prodpanels-text .range-name a {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
}


@media screen and (max-width: 768px) {
	.article-content div.container-fluid.product-grid ul.row-fluid.item-list li.span6{
		width:48%;
	}
	.article-content .product-grid li:nth-child(2n+1) .product-container, .article-content .product-grid li:nth-child(2n) .product-container {
	    margin: 0 auto;
	    max-width: 255px;
	}
}
@media screen and (max-width: 450px) {
	.article-content div.container-fluid.product-grid ul.row-fluid.item-list li.span6{
		width:100%;
		margin: 0;
	}
}