<?php 
/**
 * Plugin Name: Beaver Builder Custom Components For MLife
 * Plugin URI: http://www-wp-mat.uk.aswatson.net/
 * Description: Beaver Builder Custom Components For MLife
 * Version: 1.0
 * Author: Sabrina Leroy
 * Author URI: http://sabrina-leroy.fr
 */
 
define( 'MLIFE_BB_DIR', get_template_directory() .'/bb-components' );
define( 'MLIFE_BB_URL', get_template_directory_uri() . '/bb-components' );




function load_mlife_bb() {
    if ( class_exists( 'FLBuilder' ) ) {
        require_once 'feature-products/feature-products.php';
        require_once 'product-carousel/product-carousel.php';
        require_once 'product-slider/product-slider.php';
        require_once 'multi-add-to-basket-bundle/multi-add-to-basket-bundle.php';
    }
}
add_action( 'init', 'load_mlife_bb' );




// adding button to back end : shortcut to beaver builder (not showing anymore in the back end after last update)
add_action( 'post_submitbox_misc_actions', function( $post ){
    $link = get_permalink();
    if ( get_post_status( $post->ID ) === 'publish' ){
	    $link = $link . '?fl_builder';
    }else{
	    $link = $link . '&fl_builder';
    }
    
    echo '<div class="beaver_builder_shortcut"><a class="button button-primary button-large" href="'.$link.'">Page Builder</a></div>';
});