<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mlife-theme
 */

?>

	</div><!-- #content -->
</div><!-- #page -->

<?php wp_footer(); ?>

</div><!-- wordpress_body -->
<script>jQuery(document).ready(function(){
			$image="";
			if(jQuery("div#background-single").length){ /* if on an article page and the article has a background set*/
				$image=jQuery("div#background-single").data('src');
			}else{
				<?php  if ( get_background_image() ) { /* if there is a background image set for the website */ ?>
					$image="<?php echo get_background_image() ?>";
				<?php } ?>
			}
			if($image!=""){ /* if a background is set, either for the article or for the site */
				if(jQuery("div.backstretch").length){ /* if Hybris already had a background image set, replace */
					jQuery("div.backstretch img").attr('src','');
					jQuery("div.backstretch").css('background',"url('"+$image+"') top center scroll no-repeat");
					jQuery("div.backstretch").css('background-size',"cover");
					jQuery("div.backstretch").css('height',"100% !important");
					jQuery("div.backstretch").css('min-height',"100% !important");
					jQuery("div.backstretch").css('width',"100% !important");
				}else{/* if  not create */
					$( "body" ).append('<div class="backstretch " style="left: 0px; top: 0px; overflow: hidden; margin: 0px; padding: 0px; height: 100%;min-height: 100%; width: 100%; z-index: -999999; position: fixed;"></div>');
					jQuery("div.backstretch").css('background',"url('"+$image+"') top center scroll no-repeat");
					jQuery("div.backstretch").css('background-size',"cover");
					jQuery("div.backstretch").css('height',"100% !important");
					jQuery("div.backstretch").css('min-height',"100% !important");
					jQuery("div.backstretch").css('width',"100% !important");

				}
			}
			
		
	});
</script>
</main>
<script>

jQuery(document).ready(function() { loadCMSExtensions('<?php echo HYBRIS_URL ;?>'); })  
/* 
calling hybris replacement for its component */;</script>
<?php
	
	/*
		Show either the full or light Hybris Footer / The option is set up in BO Settings-> Hybris Preview / Generated in inc/hybris_header_footer.php
	*/
	if(get_option('hybris_preview',false)){ // if the setup was done
		$hybris_preview =  get_option('hybris_preview');
		if($hybris_preview["preview"]=="full"){ // if the setup is Full
			echo get_option('footer_hybris');
		}else{
			echo get_option('footer_hybris_light');
		}
	}else{
		echo get_option('footer_hybris_light');
	}
?>
