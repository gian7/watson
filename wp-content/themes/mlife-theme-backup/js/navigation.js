/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens
 * Sticks the menu to the top
 */



jQuery(document).ready(function(){
	
	sticky_header(); // check for sticky header when loading the page
	
	$( window ).scroll(function() {
		sticky_header(); // check for sticky header when scrolling
	});	
	
	
	$('.site').on("click",'.menu-toggle' ,function(event){
		$('.main-navigation  ul.menu').toggleClass('opened');
		$('.menu-toggle').toggleClass("rotate_clockwise");
	});
	
});

function sticky_header(){ // decide wether the header should stick or not
	var scrollTop     = $(window).scrollTop(), //where is the top of the window
	    elementOffset = $('.wordpress_body .site-header#masthead #site-navigation').offset().top, // where is the navigation
	    distance      = (elementOffset - scrollTop),
	    limit         = 98;
	
	if($( document ).width()>800){
		limit = 70;
	}
	
	
    if(distance<limit){ // if the top of the window is closer than, or under, the navigation then show the dupplicated navigation
	    if(!$('.wordpress_body .site-header#masthead-copy').length){ // if the duplicate doesn't exist
		    
		    $clone = $('.wordpress_body .site-header#masthead').clone();
		    $clone.attr('id','masthead-copy');
			$(".site#page").prepend('<div class="fixed-header-container"></div>');
			$(".fixed-header-container").prepend($clone);
	    }else{
		    $(".fixed-header-container").show();
	    }
	    $('.wordpress_body .site-header#masthead').addClass("copied");
    }else{
	    if($('.wordpress_body .site-header#masthead').hasClass("copied")){
		    $('.wordpress_body .site-header#masthead').removeClass("copied");
		    $(".fixed-header-container").hide();
	    }
    }
}

