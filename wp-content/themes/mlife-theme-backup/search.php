<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package mlife-theme
 */

get_header();
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

		

			<header class="page-header">
				<h1 class="page-title">
					<?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Search Results for: %s', 'mlife-theme' ), '<strong>' . get_search_query() . '</strong>' );
					?>
				</h1>
				<p class="search_results">
					<?php
						if($wp_query->found_posts==0){
							echo _x( 'Your search returned <strong>0</strong> results', 'mlife-theme' );
						}elseif($wp_query->found_posts==1){
							echo _x( 'Your search returned <strong>1</strong> result', 'mlife-theme' ); 
						}else{
							printf( esc_html__( 'Your search returned %s results', 'mlife-theme' ), '<strong>' . $wp_query->found_posts . '</strong>' );
						}
						
					
					?> 
				</p>
			</header><!-- .page-header -->
			
			<?php
			if ( have_posts() ) : 
			
				?>
				<div class="grid-articles grid-2">
				<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();
	
					get_template_part( 'template-parts/content', 'content' );
	
				endwhile;
				?>
				</div>
				<?php
				mlife_theme_pagination($wp_query->post_count,$wp_query->found_posts);
	
			else :
				?>
				<div class="no_results_container">
					<div class="no_results_text">
						<p>
						<?php	echo _x( "Unfortunately, your search doesn't match with any of our articles", 'mlife-theme' ); ?>
						</p>
					</div>
				</div>
				 <?php
			
		        $WPP_query_args = array(
				    'range' => 'last30days',
				    'limit' => 6,
				    'post_type' => 'post'
				);
				$WPP_query = new WPP_query( $WPP_query_args );
				$WPP_posts = $WPP_query->get_posts();
				$WPP_posts_id = array();
				
				foreach($WPP_posts as $WPP_post){
					$WPP_posts_id[] = $WPP_post->id;
				}
				
				
				$sgrid_args = array(
					'post_type' => 'post',
					'post__in' => $WPP_posts_id,
					'orderby' => 'post__in'
				);
		    	$grid_query = new WP_Query( $sgrid_args );
		    	
		    				
		

				if( $grid_query->have_posts() ){  // if the query has a result
					?>
					<div class="fake-widget widget">
						<h3 class="widget-title">
							<span class="sides"></span>
							<span class="title_table"><?php	echo _x( "Need Inspiration? You might like that ...", 'mlife-theme' ); ?></span>
							<span class="sides"></span>
						</h3>
						<div class="grid-articles grid-2">
						<?php 
							
						while( $grid_query->have_posts() ) : $grid_query->the_post();// Include the page content template.
								
							get_template_part( 'template-parts/content',  get_post_type()  );
				
						endwhile; 
						
						?>
						</div>
						
					</div>
					
					<?php
					
			
				}
			endif;
			?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_sidebar();
get_footer();
