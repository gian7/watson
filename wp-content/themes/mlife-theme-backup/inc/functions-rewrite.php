<?php 
	
	
function custom_rewrite() {
	
	add_rewrite_rule('^([^/]+)/order/([^/]+)/?$', 'index.php?category_name=$matches[1]&orderby=$matches[2]', 'top'); //   /categoryname/order/orderby
	add_rewrite_rule('^([^/]+)/order/([^/]+)/page/?([0-9]{1,})/?$', 'index.php?category_name=$matches[1]&orderby=$matches[2]&paged=$matches[3]', 'top'); //   /categoryname/order/orderby/page/pagenumber
	
	
	add_rewrite_rule('^([^/]+)/tags/([^/]+)/order/([^/]+)/?$', 'index.php?category_name=$matches[1]&filter_tag=$matches[2]&orderby=$matches[3]', 'top'); //   /categoryname/tags/tagname/order/orderby
	add_rewrite_rule('^([^/]+)/tags/([^/]+)/order/([^/]+)/page/?([0-9]{1,})/?$', 'index.php?category_name=$matches[1]&filter_tag=$matches[2]&orderby=$matches[3]&paged=$matches[4]', 'top'); //   /categoryname/tags/tagname/order/orderby/page/pagenumber
	
	
	add_rewrite_rule('^([^/]+)/tags/([^/]+)/?$', 'index.php?category_name=$matches[1]&filter_tag=$matches[2]', 'top'); //   /categoryname/tags/tagname
	add_rewrite_rule('^([^/]+)/tags/([^/]+)/page/?([0-9]{1,})/?$', 'index.php?category_name=$matches[1]&filter_tag=$matches[2]&paged=$matches[3]', 'top'); //   /categoryname/tags/tagname/page/pagenumber
	
	
	add_rewrite_rule('^tag/([^/]+)/order/([^/]+)/?$', 'index.php?tag=$matches[1]&orderby=$matches[2]', 'top'); //   /tag/tagname/order/orderby
	add_rewrite_rule('^tag/([^/]+)/order/([^/]+)/page/?([0-9]{1,})/?$', 'index.php?tag=$matches[1]&orderby=$matches[2]&paged=$matches[3]', 'top'); // /tag/tagname/order/orderby/page/pagenumber

	add_rewrite_rule('^tag/([^/]+)/page/?([0-9]{1,})/?$', 'index.php?tag=$matches[1]&paged=$matches[2]', 'top'); //   /tag/tagname/page/pagenumber
	
	add_rewrite_rule('^search/(.+)/page/?([0-9]{1,})/?$', 'index.php?s=$matches[1]&paged=$matches[2]', 'top'); //   /search/searchterm/page/pagenumber
	
	add_rewrite_rule('^(.+?)/page/?([0-9]{1,})/?$', 'index.php?category_name=$matches[1]&paged=$matches[2]', 'top'); //   /categoryname/page/pagenumber


	

	
}



add_action('init', 'custom_rewrite');

// add query vars

add_filter('query_vars', 'add_query_vars');
function add_query_vars($query_vars)
{
    $query_vars[] = 'filter_tag';
    $query_vars[] = 'orderby';
    return $query_vars;
}