<?php
/**
 * mlife-theme Theme Customizer
 *
 * @package mlife-theme
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function mlife_theme_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'mlife_theme_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'mlife_theme_customize_partial_blogdescription',
		) );
	}
	
	
	
	// back up images uploads
	
	
    $wp_customize->add_section( 'random_backup' , array(
	    'title'       => __( 'Random Backup', 'debut' ),
	    'priority'    => 30,
	    'description' => 'Upload Images to use as a Backup for Out of stock Products',
	) );
	
	$wp_customize->add_setting( 'random1', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'random1', array(
		'label'    => __( 'Image 1', 'debut' ),
		'section'  => 'random_backup',
		'settings' => 'random1',
	) ) );

	
	
	$wp_customize->add_setting( 'random2', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'random2', array(
		'label'    => __( 'Image 2', 'debut' ),
		'section'  => 'random_backup',
		'settings' => 'random2',
	) ) );
	
	
	$wp_customize->add_setting( 'random3', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'random3', array(
		'label'    => __( 'Image 3', 'debut' ),
		'section'  => 'random_backup',
		'settings' => 'random3',
	) ) );
	
	$wp_customize->add_setting( 'backup_taxonomy_select', array(
	  'capability' => 'edit_theme_options',
	  'sanitize_callback' => 'mlife_theme_sanitize_select',
	  'default' => 'post_tag',
	) );
	$wp_customize->add_control( 'backup_taxonomy_select', array(
	  'type' => 'select',
	  'section' => 'random_backup', // Add a default or your own section
	  'label' => __( 'Which Taxonomy should I use to search for Backup Images ?' ),
	  'choices' => array(
	    'post_tag' => __( 'Tags' ),
	    'category' => __( 'Categories' ),
	  ),
	) );
	
	$wp_customize->remove_section('colors');
	
	$wp_customize->remove_section('header_image');
	
}
add_action( 'customize_register', 'mlife_theme_customize_register' );





function mlife_theme_sanitize_select( $input, $setting ) {

  // Ensure input is a slug.
  $input = sanitize_key( $input );

  // Get list of choices from the control associated with the setting.
  $choices = $setting->manager->get_control( $setting->id )->choices;

  // If the input is a valid key, return it; otherwise, return the default.
  return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
}

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function mlife_theme_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function mlife_theme_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function mlife_theme_customize_preview_js() {
	wp_enqueue_script( 'mlife-theme-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}

add_action( 'customize_preview_init', 'mlife_theme_customize_preview_js' );
