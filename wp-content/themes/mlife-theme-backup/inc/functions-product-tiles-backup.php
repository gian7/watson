<?php
	
	
/**
	Create the Categories for the product tiles back up
**/
function create_post_type() {
  register_post_type( 'product_tiles_backup',
    array(
      'labels' => array(
        'name' => __( 'Product Tiles Backup' ),
        'singular_name' => __( 'Product Tiles Backup' )
      ),
      'public' => true,
      'has_archive' => false,
    )
  );
}
add_action( 'init', 'create_post_type' );


/**
 * Hybris categories to attach post to cat and skus
 */
function hybris_cat_sku() {
	
	$labels = array(
		'name'              => _x( 'SKU Hybris Taxonomies', 'taxonomy general name' ),
		'singular_name'     => _x( 'SKU Hybris Taxonomy', 'taxonomy singular name' ),
		'search_items'      => __( 'Search SKU Hybris Taxonomies' ),
		'all_items'         => __( 'All SKU Hybris Taxonomies' ),
		'parent_item'       => __( 'Parent SKU Hybris Taxonomy' ),
		'parent_item_colon' => __( 'Parent SKU Hybris Taxonomy:' ),
		'edit_item'         => __( 'Edit SKU Hybris Taxonomy' ),
		'update_item'       => __( 'Update SKU Hybris Taxonomy' ),
		'add_new_item'      => __( 'Add New SKU Hybris Taxonomy' ),
		'new_item_name'     => __( 'New SKU Hybris Taxonomy Name' ),
		'menu_name'         => __( 'SKU Hybris Taxonomy' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'sku_taxonomy' ),
	);
	
	register_taxonomy( "sku_taxonomy", array( 'post' ), $args );
	
	$labels = array(
		'name'              => _x( 'Hybris CAT Taxonomies', 'taxonomy general name' ),
		'singular_name'     => _x( 'Hybris CAT Taxonomy', 'taxonomy singular name' ),
		'search_items'      => __( 'Search CAT Hybris Taxonomies' ),
		'all_items'         => __( 'All CAT Hybris Taxonomies' ),
		'parent_item'       => __( 'Parent CAT Hybris Taxonomy' ),
		'parent_item_colon' => __( 'Parent CAT Hybris Taxonomy:' ),
		'edit_item'         => __( 'Edit CAT Hybris Taxonomy' ),
		'update_item'       => __( 'Update CAT Hybris Taxonomy' ),
		'add_new_item'      => __( 'Add New CAT Hybris Taxonomy' ),
		'new_item_name'     => __( 'New CAT Hybris Taxonomy Name' ),
		'menu_name'         => __( 'CAT Hybris Taxonomy' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'cat_taxonomy' ),
	);
	
	register_taxonomy( "cat_taxonomy", array( 'post' ), $args );
}
add_action('init', 'hybris_cat_sku', 10, 0);


