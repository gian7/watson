<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mlife-theme
 */

get_header();

	$used_ids = array(); // create table for the post id used for the slider.
	$taxo = get_queried_object(); // get current taxonomy (category or tag)
	
	$sorting = false ; 
	if(get_query_var('orderby')!=""){ // if there is a sorting in the URL
		$sorting = get_query_var('orderby');
	}
	
	
	$filter_tag = false;
	if(get_query_var('filter_tag')!=""){ // if there is a filter in URL, the filter can only be a tag 
		$filter_tag = get_query_var('filter_tag');
		$filter_tag_obj = get_term_by( 'slug' , $filter_tag ,'post_tag' );
	}
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			
			
		<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
		    <?php if(function_exists('bcn_display')) // this is for the display of the breadcrumb
		    {
		        bcn_display();
		    }?>
		</div>
		

			<header class="page-header">
				<h1 class="page-title">
					<?php
					if ( is_category() ) {
				        $title = single_cat_title( '', false );
				    } elseif ( is_tag() ) {
				        $title =  single_tag_title( '', false );
					}
				    echo $title;
				    if($filter_tag!=false){ // if the is a filter then show it next to the title
					    echo ' | <span class="filter_tag">'.$filter_tag_obj->name.'</span>';
				    }
					?>
				</h1>
				<?php
				the_archive_description( '<div class="archive-description">', '</div>' ); // show the description of the taxonomy
			
				
				
				if(get_field('posts_to_show_in_slider','term_' . $taxo->term_id)&&$filter_tag==false){ // if there is a slider for this taxonomy AND if there is no filter in the URL
					
					$posts_id = get_field('posts_to_show_in_slider','term_' . $taxo->term_id);
					$slider_args = array(
						'post_type' => 'post',
						'post__in' => $posts_id,
					);
			    	$slider_query = new WP_Query( $slider_args );
			    	if( $slider_query->have_posts() ){  // if the query has a result
					?>
					
						<div class="slick-slider">
							<div class="slide-container">
								<?php while( $slider_query->have_posts() ) : $slider_query->the_post();// Include the page content template.
									
									get_template_part( 'template-parts/content', 'slider' );
									$used_ids[] = get_the_ID();
								endwhile; ?>
							</div>
						</div>
					<?php	
						if($slider_query->found_posts>1){ // if there is more than one slide, call the slick slider libraries and init 
							wp_enqueue_script('slick-script');
							wp_enqueue_script('slick-init-script');
							wp_enqueue_style('slick-style');
							wp_enqueue_style('slick-style-theme');
						}	
					}
					
	
				}
				
				mlife_theme_filters($taxo->taxonomy,$taxo->term_id,$filter_tag,$sorting); // get the sorting and filtering
				?>
			</header><!-- .page-header -->

		
		<?php
		
		
		// build the query for the post
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1; // get the page number
			
		$sgrid_args = array(
			'posts_per_page'	=> '10',
			'post_type'		=> 'post',
			'post__not_in' => $used_ids, //exclude the ids from the slider
			'paged' => $paged,
			'tax_query' => array(
				'relation' => 'AND', // get the post for this taxonomy
				array(
					'taxonomy' => $taxo->taxonomy,
					'field'    => 'term_id',
					'terms'    => $taxo->term_id,
				),
			),
		); 

		
		if($filter_tag!=false){ // if there is a filter add it to the arguments
			$sgrid_args['tax_query'][] = array(
						'taxonomy' => 'post_tag',
						'field'    => 'slug',
						'terms'    => $filter_tag,
					);
		}
		if($sorting!=false){ // if there is a sorting add it to the arguments
			switch($sorting){
				case 'popular': // sort by most viewed, the query is very big to include the post that don't have a number of view
					$sgrid_args['orderby'] = 'meta_value_num date';
					$sgrid_args['order'] = 'DESC';
					$sgrid_args['meta_query'] = array(
				        'relation' => 'OR',
				        array( 
				            'key' => 'views_total',
				            'compare' => 'NOT EXISTS'           
				        ),
				        array( 
				            'key' => 'views_total',
				            'compare' => 'EXISTS'           
				        )
				    );
			    break;
			    case 'oldest': // sort by oldest first
			    	$sgrid_args['orderby'] = 'date';
					$sgrid_args['order'] = 'ASC';
			    break;
				default: // sort by newest first
					$sgrid_args['orderby'] = 'date';
					$sgrid_args['order'] = 'DESC';
			}
		}else{ // sort by newest first
			$sgrid_args['orderby'] = 'date';
			$sgrid_args['order'] = 'DESC';
		}
		
		
		
		// query
		$grid_query = new WP_Query( $sgrid_args );	
		if( $grid_query->have_posts() ){  // if the query has a result
			
			?>
			
			<div class="grid-articles grid-2">
			<?php 
				
			while( $grid_query->have_posts() ) : $grid_query->the_post();// Include the page content template.
					
				get_template_part( 'template-parts/content',  get_post_type()  );
	
			endwhile; 
			
			?>
			</div>
			
			
			<?php
				mlife_theme_pagination($grid_query->post_count,$grid_query->found_posts);
	
		}else {
			get_template_part( 'template-parts/content', 'none' );
			
		}
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
