<?php
/**
 * Template for displaying search forms in Twenty Sixteen
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<form role="search" method="get" class="search-form">
	<label>
		<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'mlife-theme' ); ?></span>
		<span class="mlifeicon-search"></span><input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search The Blog', 'placeholder', 'mlife-theme' ); ?> &hellip;" value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'twentysixteen' ); ?>" />
	</label>
</form>
<script>
var $ = jQuery;
	$( ".search-form" ).submit(function( event ) { /* when search is submitted go to /search/keyword instead of /?s=keyword because of Hybris case the URL needs to change */
		event.preventDefault();
		$(location).attr('href',"<?php echo esc_url( home_url( '/' ) ); ?>search/"+ convertToSlug($(this).find("input.search-field").val()));
	});
	
	function convertToSlug(Text)
	{
	    return Text
	        .toLowerCase()
	        .replace(/</g, "")
	        .replace(/>/g, "")
	        .replace(/[^A-Za-zäÄéÉöÖüÜßèÈôÔ ,-]/gi, '')
	        .replace(/\s/g, '+');
	}

	
</script> 