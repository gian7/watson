<?php

class MLifeBBProductSliderClass extends FLBuilderModule
{

    public function __construct()
    {
        parent::__construct(array(
            'name' => __('Product Slider', 'fl-builder'),
            'description' => __('Product slider display multiple products tiles at once', 'fl-builder'),
            'category' => __('Hybris MLife', 'fl-builder'),
            'dir' => MLIFE_BB_DIR . 'product-slider/',
            'url' => MLIFE_BB_URL . 'product-slider/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled' => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));

        wp_enqueue_script('slick-script');
        wp_enqueue_script('slick-init-script');
        wp_enqueue_style('slick-style');
        wp_enqueue_style('slick-style-theme');
    }

}


$hybris_categories = array('none' => 'None');
$hybris_categories_args = array(
    'posts_per_page' => '-1',
    'post_type' => 'product_tiles_backup',
    'orderby' => 'title',
    'order ' => 'ASC'
); // look for product_tiles_backup

// query
$hybris_categories_args = new WP_Query($hybris_categories_args);
if ($hybris_categories_args->have_posts()) {  // if the query has a result
    while ($hybris_categories_args->have_posts()) : $hybris_categories_args->the_post();// Include the page content template.
        $hybris_categories[get_the_ID()] = get_the_title();
    endwhile;
}


/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('MLifeBBProductSliderClass', array(
    'products' => array(
        'title' => __('Products', 'fl-builder'),
        'sections' => array(
            'general' => array(
                'title' => '',
                'fields' => array(
                    'products' => array(
                        'type' => 'form',
                        'label' => __('Product', 'fl-builder'),
                        'form' => 'product_tile_form', // ID from registered form below
                        'preview_text' => 'product', // Name of a field to use for the preview text
                        'multiple' => true,
                    ),
                ),
            ),
        ),
    ),
));
/**
 * Register a settings form to use in the "form" field type above.
 */
FLBuilder::register_settings_form('product_tile_form', array(
    'title' => __('Add Icon', 'fl-builder'),
    'tabs' => array(
        'general' => array( // Tab
            'title' => __('General', 'fl-builder'), // Tab title
            'sections' => array( // Tab Sections
                'general' => array( // Section
                    'title'         => __('Product', 'fl-builder'), // Section Title
                    'fields'        => array( // Section Fields
                        'product_id'     => array(
                            'type'          => 'text',
                            'label'         => __('Article ID', 'fl-builder'),
                            'default'       => '',
                            'maxlength'     => '',
                            'size'          => '50',
                            'placeholder'   => '47459210',
                            'class'         => '',
                            'description'   => '',
                            'help'          => 'This is the artcle ID. You can find it under the variances on the product Page, after Artikelnummer.',
                        ),
                        'select_hybris_category' => array(
                            'type'          => 'select',
                            'label'         => __( 'Hybris Category for BackUp', 'fl-builder' ),
                            'default'       => 'none',
                            'description'   => 'BackUp',

                            'options'       => $hybris_categories,
                        ),
                        'product_image'    => array(
                            'type'          => 'photo',
                            'label'         => __('Product Image', 'fl-builder'),
                            'description'   => 'If you have an high resolution image of the product you want to display, you can insert it here. If left empty, the product image from Hybris will be displayed. Use 300 X 200 Format.',
                            'help'          => 'If you have an high resolution image of the product you want to display, you can insert it here. If left empty, the product image from Hybris will be displayed. Use 300 X 200 Format.',
                        ),
                    ),
                ),
            ),
        ),
    ),
));