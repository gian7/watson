<div class="yeeeaaahhBaby">
    <div class="product-slider" id="product-slider-<?php echo $id; ?>">
        <?php foreach ($settings->products as $product): ?>
            <div class="featured-product">
                <div class="overall-container">
                    <div class="product-tile">
                        <div class="backup">
                            <?php
                            global $random_backup_img_url;
                            $alt = "";
                            $url = "";
                            $label = "";
                            $img = "";


                            // if there is no caterogy chosen on the product tile
                            if ($product->select_hybris_category == 'none') {
                                $url = HYBRIS_URL; // default is a link to the shop
                                $alt = get_the_title();
                                $label = esc_attr_x('Go to the Shop', 'mlife-theme');// default is go to the shop

                                $img = $random_backup_img_url[array_rand($random_backup_img_url)]; // get a random image from the table build either with the tags associated to the article or the theme's back up or worst case scenario a place holder image
                            } else {

                                $category_backup = array(); // create an an array

                                // get the backup images
                                $category_backup = mlife_theme_get_backup_images($product->select_hybris_category);


                                $url = get_field('url', $product->select_hybris_category);
                                $alt = get_field('label', $product->select_hybris_category);
                                $label = $alt;


                                $img = $category_backup[array_rand($category_backup)]; // get random images

                            }

                            // build the back up
                            echo '<a href="' . $url . '" title="' . $alt . '">';
                            echo '<img src="' . $img . '" alt="' . $alt . '" />';
                            if ($label != "") {
                                echo '<span class="label"><span>' . $label . '</span></span>';
                            }
                            echo '</a>';

                            ?>
                        </div>
                        <div class="instock">
                            <div class="product-image <?php if (wp_get_attachment_url($product->product_image)) {
                                echo 'no-dummy';
                            } ?>">
                                <a href="<?php echo HYBRIS_URL . "/p/" . $product->product_id; ?>"
                                   class="product-image-link">

                                    <?php
                                    if (wp_get_attachment_url($product->product_image)) {
                                        echo '<img src="' . wp_get_attachment_url($product->product_image) . '" alt="' . $product->product_id . '" />';
                                    } else { ?>
                                        <img src="{{product(<?php echo $product->product_id; ?>):primaryImageUrl}}"
                                             alt="<?php echo $product->product_id; ?>"/>
                                    <?php }
                                    ?>
                                </a>
                            </div>
                            <div class="info">
                            <span class="product-brand">
                                <span data-hybris-product-data="product(<?php echo $product->product_id; ?>):brandData.name">
                                    Brand
                                </span>
                            </span>
                                <span class="product-name">
                                <span data-hybris-product-data="product(<?php echo $product->product_id; ?>):rangeName">
                                    Product Name
                                </span>
                            </span>
                                <span class="product-desc">
                                <span data-hybris-product-data="product(<?php echo $product->product_id; ?>):name">
                                    Description of the Product
                                </span>
                            </span>
                                <span class="product-price">
                                <span class="product-price-old">
                                    <span data-hybris-product-data="product(<?php echo $product->product_id; ?>):baseOptions[0].selected.priceData.formattedValue">
                                        Price
                                    </span>
                                </span>
                                <span class="product-price-new">
                                    <span data-hybris-product-data="product(<?php echo $product->product_id; ?>):baseOptions[0].selected.igcMarkDownPrice.formattedValue">
                                        null
                                    </span>
                                </span>
                            </span>

                                <!-- DESKTOP BUTTON -->
                                <a href="/p/<?php echo $product->product_id; ?>/quickView"
                                   class="product-link quickview desktop fancybox.ajax" target="_blank"
                                   onclick="_gaq.push(['_trackEvent','CampaignPage','ShopNow','<?php echo $product->product_id; ?>']);"><span><?php echo esc_attr_x('Shop Now', 'mlife-theme'); ?></span></a>
                                <!-- DESKTOP MOBILE BUTTON -->
                                <!-- MOBILE BUTTON -->
                                <a id="addToCartButton_<?php echo $product->product_id; ?>" class="product-link mobile"
                                   type="button"
                                   onclick="openPopoverToChooseVariant('/p/<?php echo $product->product_id; ?>'); _gaq.push(['_trackEvent','CampaignPage','ShopNow','<?php echo $product->product_id; ?>']);"
                                   value="SHOP NOW">
                                    <span><?php echo esc_attr_x('Shop Now', 'mlife-theme'); ?></span>
                                </a>
                                <!-- END MOBILE BUTTON -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="slick-arrows">
    <span class="slick-prev-arrow slick-prev-arrow-<?php echo $id; ?>">
        <img src="/m-life/wp-content/themes/mlife-theme/images/left-arrow.svg" alt="left arrow">
    </span>
        <span class="slick-info-tiles">
        <span class="slick-current-group-tiles">1</span>
        of
        <span class="slick-total-group-tiles"><?php echo count($settings->products); ?></span>
    </span>
        <span class="slick-next-arrow slick-next-arrow-<?php echo $id; ?>">
                    <img src="/m-life/wp-content/themes/mlife-theme/images/right-arrow.svg" alt="left arrow">

    </span>
    </div>
</div>