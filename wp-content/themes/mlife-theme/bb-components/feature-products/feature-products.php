<?php 
	
class MLifeBBFeatureProductClass extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Feature Product', 'fl-builder' ),
            'description'     => __( 'Insert a Featured Product in you Layout', 'fl-builder' ),
            'category'        => __( 'Hybris MLife', 'fl-builder' ),
            'dir'             => MLIFE_BB_DIR . 'feature-products/',
            'url'             => MLIFE_BB_URL . 'feature-products/',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
    
}



		
$hybris_categories = array('none' => 'None');
$hybris_categories_args = array(
	'posts_per_page'	=> '-1',
	'post_type'		=> 'product_tiles_backup',
	'orderby'		=> 'title',
	'order '		=> 'ASC'
); // look for product_tiles_backup
		
// query
$hybris_categories_args = new WP_Query( $hybris_categories_args );	
if( $hybris_categories_args->have_posts() ){  // if the query has a result
	while( $hybris_categories_args->have_posts() ) : $hybris_categories_args->the_post();// Include the page content template.
		$hybris_categories[get_the_ID()] = get_the_title();
	endwhile; 
}


/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('MLifeBBFeatureProductClass', array(
    'general'       => array( // Tab
        'title'         => __('General', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'general'       => array( // Section
                'title'         => __('Product', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                	'product_id'     => array(
                        'type'          => 'text',
                        'label'         => __('Article ID', 'fl-builder'),
                        'default'       => '',
                        'maxlength'     => '',
                        'size'          => '50',
                        'placeholder'   => '47459210',
                        'class'         => '',
                        'description'   => '',
                        'help'          => 'This is the artcle ID. You can find it under the variances on the product Page, after Artikelnummer.',
                    ),
                    'select_hybris_category' => array(
					    'type'          => 'select',
					    'label'         => __( 'Product Tiles BackUp', 'fl-builder' ),
					    'default'       => 'none',
					    'description'   => 'BackUp CTA for when the product is out of stock',

					    'options'       => $hybris_categories,
					),
                    'product_image'    => array(
                        'type'          => 'photo',
                        'label'         => __('Product Image', 'fl-builder'),
                        'description'   => 'If you have an high resolution image of the product you want to display, you can insert it here. If left empty, the product image from Hybris will be displayed. Use 300 X 200 Format.',
                        'help'          => 'If you have an high resolution image of the product you want to display, you can insert it here. If left empty, the product image from Hybris will be displayed. Use 300 X 200 Format.',
                    ),
                    'select_layout' => array(
					    'type'          => 'select',
					    'label'         => __( 'Layout', 'fl-builder' ),
					    'default'       => 'layout-1col',
					    'description'   => '<br/><table><tr><td>1 Column</td><td><img src="'.MLIFE_BB_URL.'/assets/img/1col.jpg"/></td><td>1 Column + Text </td><td><img src="'.MLIFE_BB_URL.'/assets/img/1colt.jpg"/></td></tr><tr><td>2 Columns</td><td><img src="'.MLIFE_BB_URL.'/assets/img/2col.jpg"/></td><td>2 Columns + Text </td><td><img src="'.MLIFE_BB_URL.'/assets/img/2colt.jpg"/></td></tr></table>',

					    'options'       => array(
					        'layout-1col'      => __( '1 Column', 'fl-builder' ),
					        'layout-2col'      => __( '2 Columns', 'fl-builder' ),
					        'layout-1coltext'      => __( '1 Columns + Text', 'fl-builder' ),
					        'layout-2coltext'      => __( '2 Columns + Text', 'fl-builder' ),
					    ),
					    'toggle'        => array(
					        'layout-1coltext'      => array(
					            'fields'        => array( 'title', 'text_editor' ),
					        ),
					        'layout-2coltext'      => array(
					            'fields'        => array( 'title', 'text_editor' ),
					        ),
					    )
					),
                    'title'     => array(
                        'type'          => 'text',
                        'label'         => __('Title', 'fl-builder'),
                        'default'       => '',
                        'maxlength'     => '',
                        'size'          => '50',
                        'placeholder'   => 'Title',
                        'class'         => '',
                        'description'   => '',
                        'help'          => 'This is the title that will appear above the paragraph',
                    ),
                    'text_editor' => array(
					    'type'          => 'editor',
					    'media_buttons' => true,
					    'rows'          => 10
					),
					'bg_field'    => array(
                        'type'          => 'photo',
                        'label'         => __('Background Image', 'fl-builder'),
                        'description'   => 'Will stand behind the container of the product. Make sure the text stays easy to read.',
                        'help'          => 'Will stand behind the container of the product. Make sure the text stays easy to read.',
                    ),
                    'has_border' => array(
					    'type'          => 'select',
					    'label'         => __( 'Border', 'fl-builder' ),
					    'default'       => 'yes-border',
					    'description'   => 'Add a border to the product tile',
					    'options'       => array(
					        'yes-border'      => __( 'Yes', 'fl-builder' ),
					        'no-border'      => __( 'No', 'fl-builder' ),
					  	),
					),
					
                )
            )
        )
    )
));