<?php 
	
class MLifeBBProductCarouselClass extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Product Carousel', 'fl-builder' ),
            'description'     => __( 'Insert a ProductCarousel in you Layout', 'fl-builder' ),
            'category'        => __( 'Hybris MLife', 'fl-builder' ),
            'dir'             => MLIFE_BB_DIR . 'product-carousel/',
            'url'             => MLIFE_BB_URL . 'product-carousel/',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
    
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('MLifeBBProductCarouselClass', array(
    'general'       => array( // Tab
        'title'         => __('General', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'general'       => array( // Section
                'title'         => __('Carousel', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                	'carousel_id'     => array(
                        'type'          => 'text',
                        'label'         => __('Carousel ID', 'fl-builder'),
                        'default'       => '',
                        'maxlength'     => '',
                        'size'          => '50',
                        'placeholder'   => 'NewProd_Home',
                        'class'         => '',
                        'description'   => '',
                        'help'          => 'This is the carousel ID. You can find it ???? .',
                    ),
                )
            )
        )
    )
));