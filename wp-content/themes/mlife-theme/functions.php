<?php
/**
 * mlife-theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package mlife-theme
 */

if ( ! function_exists( 'mlife_theme_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function mlife_theme_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on mlife-theme, use a find and replace
		 * to change 'mlife-theme' to the name of your theme in all the template files.
		 */
		$return = load_theme_textdomain( 'mlife-theme', get_template_directory() . '/languages' );

		

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'mlife-theme' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'mlife_theme_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
		
		
	}
endif;
add_action( 'after_setup_theme', 'mlife_theme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function mlife_theme_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'mlife_theme_content_width', 640 );
}
add_action( 'after_setup_theme', 'mlife_theme_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function mlife_theme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'mlife-theme' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'mlife-theme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title"><span class="sides"></span><span class="title_table">',
		'after_title'   => '</span><span class="sides"></span></h3>',
	) );
	
	/**
	 * M-life specific sidebar for top right and left header
	 */
	 register_sidebar( array(
		'name'          => esc_html__( 'Top Right', 'mlife-theme' ),
		'id'            => 'top-right',
		'description'   => esc_html__( 'Add widgets here.', 'mlife-theme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Top Left', 'mlife-theme' ),
		'id'            => 'top-left',
		'description'   => esc_html__( 'Add widgets here.', 'mlife-theme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	
	/**
	 * M-life specific sidebar for search bar
	 */
	register_sidebar( array(
		'name'          => 'Search Bar',
		'id'            => 'search-bar',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );
	
	/**
	 * M-life specific sidebar for Space after the article
	 */
	register_sidebar( array(
		'name'          => 'Content Bottom Mlife',
		'id'            => 'content_bottom_mlife',
        'before_widget' => '<section id="%1$s" class="widget %2$s grid-articles grid-3">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title"><span class="sides"></span><span class="title_table">',
        'after_title'   => '</span><span class="sides"></span></h3>',
	) );

    /**
     * M-life specific sidebar for Space after the article
     */
    register_sidebar( array(
        'name'          => 'Content Top Mlife alternative Homepage',
        'id'            => 'content_top_tags',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '',
        'after_title'   => '',
    ) );

}
add_action( 'widgets_init', 'mlife_theme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function mlife_theme_scripts() {
	wp_enqueue_style( 'mlife-theme-style', get_stylesheet_uri()."?version=".date("Ymdhi") );
	
	wp_enqueue_style( 'raleway-css', "https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,700,800,900" );
	wp_enqueue_style( 'mlife-theme-font-awesome-5-css', 'https://use.fontawesome.com/releases/v5.0.13/css/all.css?ver=2.1.2.2' ); // fontawesome


	wp_enqueue_script( 'mlife-theme-easing', get_template_directory_uri() . '/vendor/jquery.easing.min.js', 'jquery', '20151215', true );
	wp_enqueue_script( 'mlife-theme-navigation', get_template_directory_uri() . '/js/navigation.js', 'jquery', '20151215', true );
	wp_enqueue_script( 'mlife-theme-loadmore', get_template_directory_uri() . '/js/loadmore.js', 'jquery', '20151215', true ); // load more button at the bottom of articles grid, doesn't use proper wordpress Ajax (ajax-admin.php) because Hybris blocks it
	wp_enqueue_script( 'mlife-theme-sorting', get_template_directory_uri() . '/js/sorting.js', 'jquery', '20151215', true ); // show and hide the sorting choices (maybe doesn;t need its own js but it looks tidy)
	
	// Adds masonry when sidebar is present.
	if ( is_active_sidebar( 'sidebar-1' ) ) {
		wp_enqueue_script( 'mlife-theme-masonry-pkg', get_template_directory_uri() . '/vendor/masonry-4.2.1/masonry.pkgd.min.js', 'jquery', '20151215', true );
		wp_enqueue_script( 'mlife-theme-masonry-init', get_template_directory_uri() . '/js/sidebar-masonry.js', 'jquery', '20151215', true );
		if(is_search()){// if it is a searchpage there will be a tag cloud 
			wp_enqueue_script( 'mlife-theme-tagcloud-pkg', get_template_directory_uri() . '/vendor/jquery.tx3-tag-cloud/jquery.tx3-tag-cloud.js', 'jquery', '20151215', true );
			wp_enqueue_script( 'mlife-theme-tagcloud-init', get_template_directory_uri() . '/js/tag-cloud.js', 'jquery', '20151215', true );
		}
	}
	
	if(is_single()){
		wp_enqueue_script( 'mlife-theme-single', get_template_directory_uri() . '/js/single.js', 'jquery', '20151215', true );
	}
	
	
	// gets the products from hybris 
	wp_enqueue_script('ycms-js', HYBRIS_URL.'/_ui/desktop/common/js/ycms.js', false, null, false);
	
	
	// check if product tile has promotion or is out of stock
	wp_enqueue_script( 'mlife-theme-product-tile', get_template_directory_uri() . '/js/product-tile.js', 'jquery', '20151215', true );
	
	

	wp_enqueue_script( 'mlife-theme-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	
	/**
	 * Regitering Slick in case we need it later
	 */
	wp_register_style('slick-style', get_template_directory_uri() . '/vendor/slick-1.8.1/slick.css'); 
	wp_register_style('slick-style-theme', get_template_directory_uri() . '/vendor/slick-1.8.1/slick-theme.css'); 
	
	wp_register_script( 'slick-script', get_template_directory_uri() . '/vendor/slick-1.8.1/slick.min.js', 'jquery', '20151215', true );
	wp_register_script( 'slick-init-script', get_template_directory_uri() . '/js/slick-init.js', 'slick-script', '20151215', true );
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'mlife_theme_scripts' );


/**
 * Jquery assets for admin back-office because those files were emptied in the wordpress core folder to allow Hybris compatibility
 */
function load_admin_script() {
	echo "<script type='text/javascript' src='".get_template_directory_uri()."/js/admin/jquery.js'></script>";
	wp_enqueue_style('admin-style', get_template_directory_uri() . '/css/admin.css'); 
}
add_action( 'admin_enqueue_scripts', 'load_admin_script', 1 ); 

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/theme-customizer.php';

/**
 * Load Jetpack compatibility file.
 */
//if ( defined( 'JETPACK__VERSION' ) ) {
//	require get_template_directory() . '/inc/jetpack.php';
//}

/**** End of underscores ****/



/**
 * Generate the header and footer from Hybris to create a preview.
 */
require get_template_directory() . '/inc/hybris-header-footer.php';


/**
 * Add an icon or colour class to taxonomy menu
 */
require get_template_directory() . '/inc/taxonomy-walker-menu.php';


/**
 * Functions for the ACF fields
 */
require get_template_directory() . '/inc/functions-acf.php';


/**
 * Functions for rewriting URLs
 */
require get_template_directory() . '/inc/functions-rewrite.php';


/**
 * Functions popular post widgets
 */
require get_template_directory() . '/inc/widget-mlifePopularPosts.php';


/**
 * Functions Mlife Category and tag links
 */
require get_template_directory() . '/inc/widget-mlifeCategoryTagLinks.php';

/**
 * Functions Mlife Top Tag Header Homepage
 */
require get_template_directory() . '/inc/widget-mlifeTopTagHeaderHomepage.php';

/**
 * Functions popular post widgets
 */
require get_template_directory() . '/inc/functions-related-post-mlife.php';

/**
 * Functions Hybris Cat Skus, and backup for out of stock products
 */
require get_template_directory() . '/inc/functions-product-tiles-backup.php';



/**
 * Functions BB Components
 */
require get_template_directory() . '/bb-components/mlife-bb-components.php';
