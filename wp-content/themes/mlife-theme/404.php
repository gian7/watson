<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package mlife-theme
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><span>404</span> <?php esc_html_e( 'Oops!', 'mlife-theme' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php esc_html_e( "It looks like this page doesn't exist anymore", 'mlife-theme' ); ?> <br/>
					 <?php esc_html_e( "Are you looking for something specific?", 'mlife-theme' ); ?></p>
					
					<p class="no-margin"><?php esc_html_e( "Try it with our search function", 'mlife-theme' ); ?> </p>
					<div id="search-404">
						
						<?php get_search_form(); ?>
					</div>
				</div><!-- .page-content -->
				<?php
				
				$WPP_query_args = array(
				    'range' => 'last30days',
				    'limit' => 3,
				    'post_type' => 'post'
				);
				$WPP_query = new WPP_query( $WPP_query_args );
				$WPP_posts = $WPP_query->get_posts();
				$WPP_posts_id = array();
				
				foreach($WPP_posts as $WPP_post){
					$WPP_posts_id[] = $WPP_post->id;
				}
				
				
				$sgrid_args = array(
					'post_type' => 'post',
					'post__in' => $WPP_posts_id,
					'orderby' => 'post__in'
				);
		    	$grid_query = new WP_Query( $sgrid_args );
		    	
		    				
		

				if( $grid_query->have_posts() ){  // if the query has a result
					?>
					<div class="fake-widget widget">
						<h3 class="widget-title">
							<span class="sides"></span>
							<span class="title_table"><?php	echo _x( "Discover the Trends ...", 'mlife-theme' ); ?></span>
							<span class="sides"></span>
						</h3>
						<div class="grid-articles grid-3">
						<?php 
							
						while( $grid_query->have_posts() ) : $grid_query->the_post();// Include the page content template.
								
							get_template_part( 'template-parts/content',  get_post_type()  );
				
						endwhile; 
						
						?>
						</div>
						
					</div>
					
					<?php
					
			
				}
			?>
			</section><!-- .error-404 -->

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>