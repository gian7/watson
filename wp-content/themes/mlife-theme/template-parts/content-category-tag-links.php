<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mlife-theme
 */
?>
<div class="menu-see-all-container">
    <ul id="menu-see-all" class="menu">
        <?php foreach ($tags as $tag) : ?>
            <?php
            if (($current_category instanceof WP_Error) || ($current_category == null)) {
                $href = home_url() . DIRECTORY_SEPARATOR . 'tag' . DIRECTORY_SEPARATOR . $tag->slug;
            } else {
                $href = home_url() . DIRECTORY_SEPARATOR . $current_category->slug . DIRECTORY_SEPARATOR . 'tags' . DIRECTORY_SEPARATOR . $tag->slug;
            }
             ?>
            <?php $classes = mlife_theme_term_customisation($tag->term_id) ?>
            <li id="menu-item-<?php echo $tag->term_id ?>"
                class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-<?php echo $tag->term_id ?> <?php echo $classes; ?>">
                <a href="<?php echo $href; ?>"
                   title="<?php echo $tag->name; ?>">
                    <?php echo $tag->name; ?>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>