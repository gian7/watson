<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mlife-theme
 */
?>
    <div class="home_makeup">
        <div class="col2">
            <a href="<?php echo $cta_link; ?>" class="one_square" style="background-image: url(<?php echo $main_image; ?>)">
				<span>
					<img src="<?php echo home_url();?>/wp-content/themes/mlife-theme/images/logo_make_up_life.png">
					<span class="logo_hover"><img src="<?php echo home_url();?>/wp-content/themes/mlife-theme/images/logo_make_up_life_hover.png"></span>
					<span class="cta"><?php echo $cta_text; ?></span>
				</span>
            </a>
        </div>
        <div class="col1">

            <a href="<?php echo home_url() .  DIRECTORY_SEPARATOR . 'tag'. DIRECTORY_SEPARATOR .$tiles[0]['tag']->slug ?>" class="four_square " style="background-image: url(<?php echo $tiles[0]['image']?>)">
                <span><?php echo $tiles[0]['tag']->name ?></span>
            </a>

            <a href="<?php echo home_url() .  DIRECTORY_SEPARATOR . 'tag'. DIRECTORY_SEPARATOR .$tiles[2]['tag']->slug ?>" class="four_square " style="background-image: url(<?php echo $tiles[2]['image']?>)">
                <span><?php echo $tiles[2]['tag']->name ?></span>
            </a>
        </div>
        <div class="col1">
            <a href="<?php echo home_url() .  DIRECTORY_SEPARATOR . 'tag'. DIRECTORY_SEPARATOR .$tiles[1]['tag']->slug ?>" class="four_square" style="background-image: url(<?php echo $tiles[1]['image']?>)">
                <span><?php echo $tiles[1]['tag']->name ?></span>
            </a>

            <a href="<?php echo home_url() .  DIRECTORY_SEPARATOR . 'tag'. DIRECTORY_SEPARATOR .$tiles[3]['tag']->slug ?>" class="four_square" style="background-image: url(<?php echo $tiles[3]['image']?>)">
                <span><?php echo $tiles[3]['tag']->name ?></span>
            </a>
        </div>
        <div class="clear"></div>
    </div>