<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mlife-theme
 */

?>

<article id="popular-grid-post-<?php the_ID(); ?>" <?php post_class("article-grid"); ?>>
	<header class="entry-header">
		<?php
			$url =  wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium');
		?>
		<div class="thumbnail-grid <?php echo (buIsMCH() ? 'thumbnail-grid-mch' : '') ?>" style="<?php if($url){ echo "background-image: url('".$url[0]."')";} ?>">
			<a href="<?php the_permalink(); ?>" class="<?php echo $tab_cat->slug; ?>" title="<?php the_title(); ?>">
			
			</a>
		</div>
		<div class="info-popular-grid">
			<h4 class="entry-title"><a href="<?php the_permalink(); ?>" >
		<?php 
			$title = get_the_title();
			if(strlen($title)>40) {
				echo substr ($title, 0, 40) ."...";
			}else{the_title();}
					
			?></a></h4>
			<a href="<?php the_permalink(); ?>" class="read-more">
				<?php echo _x( 'read more', 'mlife-theme' ); ?>
			</a>
		</div>
	</header><!-- .entry-header -->


	
	<footer class="entry-footer">
			<?php echo mlife_theme_theme_get_the_tag_list_dots();?>

		<?php mlife_theme_entry_date(); ?>
		<div class="clear"></div>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
