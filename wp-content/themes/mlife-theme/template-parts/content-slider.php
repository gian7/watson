<?php
/**
 * The template part for displaying content in post sliders
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mlife-theme
 */
?>

<div id="slider-post-<?php the_ID(); ?>" <?php post_class(); ?> >
	<?php $image = get_field('header_image');  ?>
	
	<div class="thumbnail-grid <?php echo (buIsMCH() ? 'thumbnail-grid-mch' : '') ?>" style="<?php if($image){ echo "background-image: url('".$image['sizes']['large']."')";} ?>">
		<div class="white-banner">
				<h2 class="entry-title">
	<?php 
		$title = get_the_title();
		if(strlen($title)>60) {
			echo substr ($title, 0, 60) ."...";
		}else{the_title();}
				
		?></h2>
			<a href="<?php the_permalink(); ?>" class="read-more">
				<?php echo _x( 'Read More', 'mlife-theme' ); ?>
			</a>
		</div>
			
	</div>
</div><!-- #post-## -->
