<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mlife-theme
 */

?>

<article id="grid-post-<?php the_ID(); ?>" <?php post_class("article-grid"); ?>>
	<header class="entry-header">
		<?php
			$url =  wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium');
		?>
		<div class="thumbnail-grid <?php echo (buIsMCH() ? 'thumbnail-grid-mch' : '') ?>" style="<?php if($url){ echo "background-image: url('".$url[0]."')";} ?>">
			<?php 
				// get the primary category
				$cat = new WPSEO_Primary_Term('category', get_the_ID()); 
				$cat = $cat->get_primary_term();
				if($cat){
					$tab_cat = get_category($cat); 
					
				}else{
					$categories = get_the_category();
					$tab_cat = $categories[0]; 
				}
				
			?>
			<a href="<?php the_permalink(); ?>" class="<?php echo $tab_cat->slug; ?>" title="<?php the_title(); ?>">
				
				<span class="overlay">
					<span class="entry-info">
						<span class="category"><?php echo $tab_cat->name; ?></span>
						<span class="hr"></span>
						<span class="excerpt">
							<?php 
								$excerpt = "";
								$excerpt = strip_tags(get_excerpt_by_id($post->ID));
								if(strlen($excerpt)>70) {
									echo substr ($excerpt, 0, 70) ."...";
								}else{ echo $excerpt;} 	
								$excerpt = "";
							?>
						</span>
					</span>
				</span>
			</a>
		</div>
		<h2 class="entry-title"><a href="<?php the_permalink(); ?>">
	<?php 
		$title = get_the_title();
		if(strlen($title)>60) {
			echo substr ($title, 0, 60) ."...";
		}else{the_title();}
				
		?></a></h2>
		<div class="entry-button-container">
			<a href="">
				<button class="entry-button">
					Lorem ipsum
				</button>
			</a>
		</div>
	</header><!-- .entry-header -->


	
	<footer class="entry-footer">
		<div class="entry-category-icon"></div>
		<?php mlife_theme_entry_date(); ?>
		<div class="tags-container">
			<?php echo mlife_theme_theme_get_the_tag_list_dots();?>
		</div>
		<!-- <a href="<?php //the_permalink(); ?>" class="read-more">
			<?php //echo _x( 'Read More', 'mlife-theme' ); ?>
		</a> -->
		<!-- <div class="clear"></div> -->
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
