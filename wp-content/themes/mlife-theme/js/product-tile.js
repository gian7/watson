jQuery(document).ready(function() {
	setTimeout(function(){
		jQuery(".product-tile .product-price").each(function() {
			var new_price=jQuery(this).children(".product-price-new");
			var old_price=jQuery(this).children(".product-price-old");
			/*alert("_"+new_price.html().toLowerCase()+"_"+new_price.html().toLowerCase().search("null"));*/
			if (jQuery("html").hasClass("msie8")) {
				new_price_html =new_price.html().toLowerCase();
				new_price_html =new_price_html.replace("<span>","");
				new_price_html =new_price_html.replace("</span>","");
				new_price_html =new_price_html.replace("<span>","");
				new_price_html =new_price_html.replace(" ","");
				new_price_html =new_price_html.trim();
				console.log("IE8 :"+ new_price_html);
			} else {
				if (new_price.html().toLowerCase().search("null") <= 0){
					new_price.css("display","inline-block");
					new_price.css("color","#ee3680");
					old_price.css("text-decoration","line-through");
					old_price.css("color","#9d9d9d");
					old_price.css("font-weight","400");
					old_price.css("font-size","15px");
				}
			}
			
		});
	}, 2000);
	
	setTimeout(function(){
		jQuery(".product-tile").each(function() {
			
			var productName = jQuery(this).find(".product-name");
			var productName_html = productName.html();
			if(jQuery.type(productName_html) != "undefined"){
				
				if(productName_html.indexOf("data-hybris-product-data") != -1){
				    jQuery(this).addClass("out_of_stock");
				}
			}
			
		});
	}, 2000);
});