<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mlife-theme
 */

get_header();
?>

<?php

if ( function_exists('dynamic_sidebar') && is_active_sidebar("content_top_tags") ) : ?>
    <div id="content_top_tags" class="content_top_tags" role="complementary">
        <?php dynamic_sidebar( 'content_top_tags' ); ?>
    </div>
<?php else : ?>
    <?php
        $used_ids = array();
		$slider_args = array(
			'posts_per_page'	=> '-1',
			'post_type'		=> 'post',
			'meta_key'		=> 'add_to_home_slider',
			'meta_value'	=> '1'
		); // look for posts that are supposed to be on homepage slider 
		
		// query
		$slider_query = new WP_Query( $slider_args );	
		if( $slider_query->have_posts() ){  // if the query has a result
	?>
		<div class="slick-slider">
			<div class="slide-container">
				<?php while( $slider_query->have_posts() ) : $slider_query->the_post();// Include the page content template.
					
					get_template_part( 'template-parts/content', 'slider' );
					$used_ids[] = get_the_ID();
				endwhile; ?>
			</div>
		</div>
	<?php	
			if($slider_query->found_posts>1) { // if there is more than one slide, call the slick slider libraries and init
				wp_enqueue_script('slick-script');
				wp_enqueue_script('slick-init-script');
				wp_enqueue_style('slick-style');
				wp_enqueue_style('slick-style-theme');
			}	
		}
	?>
    <?php endif; ?>
	
	
	
	


	
	
	
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		<?php
			function js_log($variable){
				?><script>console.log(<?php echo json_encode($variable); ?>);</script><?php
			}
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 10;
			
		$sgrid_args = array(
			'posts_per_page'	=> '10',
			'post_type'		=> 'post',
			//'paged' => $paged,
			'post_not_in' => $used_ids,
			'meta_key'		=> 'add_to_home_slider',
			'meta_value'	=> '0',
			//'post__not_in' => $used_ids
	
		); // look for posts that are not supposed to be on homepage slider 
		
		// query
		$grid_query = new WP_Query( $sgrid_args );	
		if( $grid_query->have_posts() ){  // if the query has a result
			?>
			<div class="grid-articles grid-2">
			<?php 
				
			while( $grid_query->have_posts() ) : $grid_query->the_post();// Include the page content template.
					//js_log(wp_get_upload_dir());
				get_template_part( 'template-parts/content',  get_post_type()  );
	
			endwhile; 
			
			?>
			</div>
			
			
			<?php
				mlife_theme_pagination(1,10); // get the pagination (for google) and load more button (for users)
	
		}else {
			get_template_part( 'template-parts/content', 'none' );
			
		}
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();