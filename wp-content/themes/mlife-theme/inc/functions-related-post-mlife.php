<?php
// Override the manual related post display for post 	
	
function manual_related_posts_MLife($atts){
	global $post;
	$relatedposts = bawmrp_get_all_related_posts($post);
	if(!empty( $relatedposts)){
		ob_start();
		global $post;
		foreach( $relatedposts as $post ) : 
			setup_postdata($post);
			get_template_part( 'template-parts/content', 'content' );
		endforeach; 
		echo '<div class="clear"></div>';
	} 
	$output =  ob_get_clean();
	return $output;
	wp_reset_query();	 // Restore global post data stomped by the_post().
}
add_shortcode('manual_related_posts_MLife','manual_related_posts_MLife'); // functions-related-post-mlife.phpfunctions-related-post-mlife.php  


// This function was taken from stackoverflow, because of  bug in Wordpress : at the bottom of the post all the related posts had the excerpt of the current post ....
function get_excerpt_by_id($post_id){
    $the_post = get_post($post_id); //Gets post ID
    $the_excerpt = $the_post->post_content; //Gets post_content to be used as a basis for the excerpt
    $excerpt_length = 35; //Sets excerpt length by word count
    $the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
    $words = explode(' ', $the_excerpt, $excerpt_length + 1);

    if(count($words) > $excerpt_length) :
        array_pop($words);
        array_push($words, '...');
        $the_excerpt = implode(' ', $words);
    endif;

    $the_excerpt = '<p>' . $the_excerpt . '</p>';

    return $the_excerpt;
}
