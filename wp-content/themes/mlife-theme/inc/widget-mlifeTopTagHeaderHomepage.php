<?php
/**
 * Functions which enhance the theme by creating more widgets into WordPress
 The fields are added with advanced custom fields
 *
 * @package mlife_theme
 */
/**
 * Adds Links to category or tags depending on landing page in the sidebar widget.
 */
class mlifeTopTagHeaderHomepage extends WP_Widget {
	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'mlifeTopTagHeaderHomepage', // Base ID
			esc_html__( 'Mlife Top Tag Header Homepage', 'text_domain' ), // Name
			array( 'description' => esc_html__( 'This is used on MRO as a replacement of the silder it contains links of tags related to makeup', 'text_domain' ), ) // Args
		);
		
	}
	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
        $tiles = get_field('tiles','widget_'.$args['widget_id']);
        $main_image = get_field('main_image','widget_'.$args['widget_id']);
        $cta_text = get_field('cta_text','widget_'.$args['widget_id']);
        $link = get_field('cta_link','widget_'.$args['widget_id']);
        $cta_link = home_url() . DIRECTORY_SEPARATOR . 'tag'. DIRECTORY_SEPARATOR . $link->slug;
        // Include template passing all the variables above
        include( locate_template( 'template-parts/content-top-tag-header-homepage.php', false, true ) );

		echo $args['after_widget'];
	}
	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		
	}
	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		
	}
	
} // class mlifeTopTagHeaderHomepage
// register mlifeTopTagHeaderHomepage widget
function register_mlifeTopTagHeaderHomepage() {
    register_widget( 'mlifeTopTagHeaderHomepage' );
}
add_action( 'widgets_init', 'register_mlifeTopTagHeaderHomepage' );

