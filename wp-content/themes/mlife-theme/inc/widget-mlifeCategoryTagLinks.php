<?php
/**
 * Functions which enhance the theme by creating more widgets into WordPress
 The fields are added with advanced custom fields
 *
 * @package mlife_theme
 */
/**
 * Adds Links to category or tags depending on landing page in the sidebar widget.
 */
class mlifeCategoryTagLinks extends WP_Widget {
	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'mlifeCategoryTagLinks', // Base ID
			esc_html__( 'Mlife Category and tag links', 'text_domain' ), // Name
			array( 'description' => esc_html__( 'Links to category or tags depending on landing page in the sidebar', 'text_domain' ), ) // Args
		);
		
	}
	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		$current_category = get_category( get_query_var( 'cat' ) );

        $title = get_field('title','widget_'.$args['widget_id']);
        $tags = get_field('tags','widget_'.$args['widget_id']);


        if(get_field('title','widget_'.$args['widget_id'])){
            echo $args['before_title'].get_field('title','widget_'.$args['widget_id']).$args['after_title'];
        }
        // Include template passing all the variables above
        include( locate_template( 'template-parts/content-category-tag-links.php', false, true ) );

		echo $args['after_widget'];
	}
	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		
	}
	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		
	}
	
} // class mlifeCategoryTagLinks
// register mlifeCategoryTagLinks widget
function register_mlifeCategoryTagLinks() {
    register_widget( 'mlifeCategoryTagLinks' );
}
add_action( 'widgets_init', 'register_mlifeCategoryTagLinks' );

