<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package mlife_theme-theme
 */

if ( ! function_exists( 'mlife_theme_theme_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 */
	function mlife_theme_theme_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( DATE_W3C ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( DATE_W3C ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( 'Posted on %s', 'post date', 'mlife_theme-theme' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		echo '<span class="posted-on">' . $posted_on . '</span>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'mlife_theme_theme_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 */
	function mlife_theme_theme_posted_by() {
		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( 'by %s', 'post author', 'mlife_theme-theme' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

	}
endif;


if ( ! function_exists( 'mlife_theme_theme_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function mlife_theme_theme_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ', ', 'mlife_theme-theme' ) );
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'mlife_theme-theme' ) . '</span>', $categories_list ); // WPCS: XSS OK.
			}

			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'mlife_theme-theme' ) );
			if ( $tags_list ) {
				/* translators: 1: list of tags. */
				printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'mlife_theme-theme' ) . '</span>', $tags_list ); // WPCS: XSS OK.
			}
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link(
				sprintf(
					wp_kses(
						/* translators: %s: post title */
						__( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'mlife_theme-theme' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				)
			);
			echo '</span>';
		}

		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Edit <span class="screen-reader-text">%s</span>', 'mlife_theme-theme' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
endif;

if ( ! function_exists( 'mlife_theme_theme_post_thumbnail' ) ) :
	/**
	 * Displays an optional post thumbnail.
	 *
	 * Wraps the post thumbnail in an anchor element on index views, or a div
	 * element when on single views.
	 */
	function mlife_theme_theme_post_thumbnail() {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular() ) :
			?>

			<div class="post-thumbnail">
				<?php the_post_thumbnail(); ?>
			</div><!-- .post-thumbnail -->

		<?php else : ?>

		<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
			<?php
			the_post_thumbnail( 'post-thumbnail', array(
				'alt' => the_title_attribute( array(
					'echo' => false,
				) ),
			) );
			?>
		</a>

		<?php
		endif; // End is_singular().
	}
endif;




if ( ! function_exists( 'mlife_theme_theme_get_the_tag_list_dots' ) ) :

/**
 * Retrieve the tags for a post formatted as a string. Override Wordpress tag list for having the dots
 */
function mlife_theme_theme_get_the_tag_list_dots( $before = '', $sep = '', $after = '', $id = 0 ) {
	
	return apply_filters( 'the_tags', mlife_theme_get_the_term_list_dots( $id, 'post_tag', $before, $sep, $after ), $before, $sep, $after, $id );
}

endif;
if ( ! function_exists( 'mlife_theme_get_the_term_list_dots' ) ) :

/**
 * Retrieve a post's terms as a list with specified format. Override Wordpress tag list for having the dots
 */
function mlife_theme_get_the_term_list_dots( $id, $taxonomy, $before = '', $sep = '', $after = '' ) {
	$terms = get_the_terms( $id, $taxonomy ); // get all the tags
	//js_log($terms);
	if ( is_wp_error( $terms ) )
		return $terms;

	if ( empty( $terms ) )
		return false;

	$links = array();

	foreach ( $terms as $term ) { // for each tags construct the dot link 
			
		
		$link = get_term_link( $term, $taxonomy );
		if ( is_wp_error( $link ) ) {
			return $link;
		}
		
		$links[] = '<a class="'.$term->slug.' dot-tags '.mlife_theme_term_customisation($term->term_id).'" href="' . esc_url($link) . '" rel="tag" title="'.$term->name.'"></a>';
		
	}

	/**
	 * Filter the term links for a given taxonomy.
	 *
	 * The dynamic portion of the filter name, `$taxonomy`, refers
	 * to the taxonomy slug.
	 *
	 * @since 2.5.0
	 *
	 * @param array $links An array of term links.
	 */
	$term_links = apply_filters( "term_links-$taxonomy", $links );

	return $before . join( $sep, $term_links ) . $after;
}
endif;
if ( ! function_exists( 'mlife_theme_get_the_tag_list' ) ) :
function mlife_theme_get_the_tag_list( $before = '', $sep = '', $after = '', $id = 0 ) {

	/**
	 * Filter the tags list for a given post.
	 */
	return apply_filters( 'the_tags', mlife_theme_get_the_term_list( $id, 'post_tag', $before, $sep, $after ), $before, $sep, $after, $id );
}

endif;
if ( ! function_exists( 'mlife_theme_get_the_term_list' ) ) :
/**
 * Retrieve a post's terms as a list with specified format.
 */
function mlife_theme_get_the_term_list( $id, $taxonomy, $before = '', $sep = '', $after = '' ) {
	
	$terms = get_the_terms( $id, $taxonomy );

	if ( is_wp_error( $terms ) )
		return $terms;

	if ( empty( $terms ) )
		return false;

	$links = array();

	foreach ( $terms as $term ) {
		$link = get_term_link( $term, $taxonomy );
		if ( is_wp_error( $link ) ) {
			return $link;
		}
		$links[] = '<a class="'.$term->slug.' terms '.mlife_theme_term_customisation($term->term_id).'" href="' . esc_url( truncate_url($link )) . '" rel="tag" title="'.$term->name.'">'.$term->name.'</a>';
		
		global $backup_taxonomy_select;
		if($backup_taxonomy_select=="post_tag"){
			// get the backup images
			global $random_backup_img_url;
			$random_backup_img_url = array_merge ($random_backup_img_url, mlife_theme_get_backup_images('term_' . $term->term_id));
		}
		
	}

	/**
	 * Filter the term links for a given taxonomy.
	 */
	$term_links = apply_filters( "term_links-$taxonomy", $links );

	return $before . join( $sep, $term_links ) . $after;
}

endif;

if ( ! function_exists( 'mlife_theme_get_backup_images' ) ) :

/**
 * return the array of back up images attached to the object (taxonomy or custom type product tile backup) 
 */
function mlife_theme_get_backup_images($object_id = false) {
	$img_array = array();
	if($object_id != false){
		$images = get_field('backup_images',$object_id);
		$size = 'full'; // (thumbnail, medium, large, full or custom size)
		
		if( $images ){
			foreach( $images as $image ){
				$img_array[] = $image['url'];
			}
		}	
	}	
	return $img_array;
}

endif;

if ( ! function_exists( 'mlife_theme_entry_date' ) ) :
/**
 * Prints HTML with date information for current post.
 *
 * Create your own twentysixteen_entry_date() function to override in a child theme.
 *
 * @since Twenty Sixteen 1.0
 */
function mlife_theme_entry_date($id = 0) {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ,$id) !== get_the_modified_time( 'U',$id ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}
	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ,$id) ),
		get_the_date('',$id),
		esc_attr( get_the_modified_date( 'c' ,$id) ),
		get_the_modified_date('',$id)
	);
	printf( '<span class="posted-on"><span class="screen-reader-text">%1$s </span><a href="%2$s" rel="bookmark">%3$s</a></span>','',
		esc_url( get_permalink($id) ),
		$time_string
	);
}
endif;





if ( ! function_exists( 'mlife_theme_filters' ) ) :
/*

list the filters
tags if Category page
categories if tag page 

No AJAX no multiple filters

 */
function mlife_theme_filters($type = 'all', $current = false, $filter = false, $sorting = false) {
	
	if($type == "category" && $current){ // If the type of page displayedis a category
		
		$category_url = get_category_link($current); // get the link of this category to build the sub page later
		
		query_posts('cat='.$current.'&showposts=-1');
		// make a list of all the tags from the articles shown
		if (have_posts()) : while (have_posts()) : the_post();
	        $posttags = get_the_tags();
			if ($posttags) {
				foreach($posttags as $tag) {
					$all_tags_arr[$tag->term_id] = array( 	'slug' => $tag->slug,
															'name' => $tag->name); //USING JUST $tag MAKING $all_tags_arr A MULTI-DIMENSIONAL ARRAY
				}
			}
		endwhile; endif; 

		
		foreach($all_tags_arr as $tag_id=>$tag_arr) { // for each tag, create a <a> with custom style and cat url + tags + tag name 
			
			$class_selected = "";
			if($tag_arr['slug']== $filter){
				$class_selected = 'selected';
			}// if the tag is equal to the one used to filter then add a class selected
			
			
			
	
			$tag_links .=  '<a class="'.$tag_arr['slug'].' tags terms '.$class_selected.' '.mlife_theme_term_customisation($tag_id).'  " href="' . esc_url($category_url) . 'tags/'.$tag_arr['slug'].'" rel="tag" title="'.$tag_arr['name'].'">'.$tag_arr['name'].'</a>';
			
		}
		
		
		echo '<div class="tag-list">'.$tag_links.'</div>';
		
		$base_url = $category_url; // build the URL for the sorting
		
		if($filter!=false){ // if there is already a filter in place then add it to the base URL
			$base_url = $base_url.'tags/'.$filter.'/';
		}
		
		?>
		<div class="sorting_select">
			<span class="sorting_label"><?php echo _x( 'Sort by :', 'mlife-theme' ); ?></span>
			<span class="sorting_choice">
			<?php
				if($sorting!=false){
					switch($sorting){
						case 'recent':
							echo _x( 'Most Recent', 'mlife-theme' );
							break; 
						case 'popular':
							echo _x( 'Most Popular', 'mlife-theme' );
							break; 
						case 'oldest':
							echo _x( 'Oldest', 'mlife-theme' );
							break; 
						default:
							echo _x( 'Most Recent', 'mlife-theme' );
					}
				}else{
					echo _x( 'Most Recent', 'mlife-theme' );
				}
			?>
			</span>
			
			<ul>
				<li><a href="<?php echo $base_url.'order/recent'; ?>"><?php echo _x( 'Most Recent', 'mlife-theme' ); ?></a></li>
				<li><a href="<?php echo $base_url.'order/popular'; ?>"><?php echo _x( 'Most Popular', 'mlife-theme' ); ?></a></li>
				<li><a href="<?php echo $base_url.'order/oldest'; ?>"><?php echo _x( 'Oldest', 'mlife-theme' ); ?></a></li>
			</ul>
		</div>
		<div class="clear"></div>
		<?php 
		wp_reset_query();	 // Restore global post data stomped by the_post().
	}
	if($type == "post_tag" && $current){ // If the type of page displayedis a tag
		
		$current_tag = get_term($current); // get current term 
		
		query_posts('tag_id='.$current_tag->term_id.'&showposts=-1');
		// make a list of all the tags from the articles shown
		if (have_posts()) : while (have_posts()) : the_post();
		
			$postcats = get_the_category(); 
			
			
			if ($postcats) {
				foreach($postcats as $cat) {
					$all_cats_arr[$cat->term_id] = array( 	'slug' => $cat->slug,
															'name' => $cat->name); //USING JUST $tag MAKING $all_tags_arr A MULTI-DIMENSIONAL ARRAY				
														}
			}
		endwhile; endif; 

		
		foreach($all_cats_arr as $cat_id=>$cat_arr) {			
			
			
			$category_url = get_category_link($cat_id); // get the link of this category to build the sub page
			

			$cat_links .=  '<a class="'.$cat_arr['slug'].' tags terms '.mlife_theme_term_customisation($cat_id).'  " href="' . esc_url($category_url) . 'tags/'.$current_tag->slug.'" rel="tag" title="'.$cat_arr['name'].'">'.$cat_arr['name'].'</a>';
			
		}
		
		
		echo '<div class="tag-list">'.$cat_links.'</div>';
		
		
		
		$base_url = get_tag_link($current_tag->term_id); // base URL for the sorting
				
		?>
		<div class="sorting_select">
			<span class="sorting_label"><?php echo _x( 'Sort by :', 'mlife-theme' ); ?></span>
			<span class="sorting_choice">
			<?php
				if($sorting!=false){
					switch($sorting){
						case 'recent':
							echo _x( 'Most Recent', 'mlife-theme' );
							break; 
						case 'popular':
							echo _x( 'Most Popular', 'mlife-theme' );
							break; 
						case 'oldest':
							echo _x( 'Oldest', 'mlife-theme' );
							break; 
						default:
							echo _x( 'Most Recent', 'mlife-theme' );
					}
				}else{
					echo _x( 'Most Recent', 'mlife-theme' );
				}
			?>
			</span>
			
			<ul>
				<li><a href="<?php echo $base_url.'order/recent'; ?>"><?php echo _x( 'Most Recent', 'mlife-theme' ); ?></a></li>
				<li><a href="<?php echo $base_url.'order/popular'; ?>"><?php echo _x( 'Most Popular', 'mlife-theme' ); ?></a></li>
				<li><a href="<?php echo $base_url.'order/oldest'; ?>"><?php echo _x( 'Oldest', 'mlife-theme' ); ?></a></li>
			</ul>
		</div>
		<div class="clear"></div>
		<?php 
		wp_reset_query();	 // Restore global post data stomped by the_post().
	}
}
endif;






if ( ! function_exists( 'mlife_theme_pagination' ) ) :
/**
 * load more pagination
 */
function mlife_theme_pagination($initial_number_posts = 0,$total_number_posts = 0){
	
	if($total_number_posts > 10){
	?>
		<div class="loadmore-container">
			
			<div class="number_posts">
				<span class="post_showing"><?php echo $initial_number_posts; ?></span> 
				<?php echo _x( 'of', 'mlife-theme' ); ?>
				<span class="total_post"><?php echo $total_number_posts; ?></span>
				
			</div>
			<a class="load-more-button"> 
				<span class="loading-line">
					<?php echo _x( 'Show More Articles', 'mlife-theme' ); ?>
				</span>
			</a>
			<div class="no-more">
				<?php echo _x( 'No more to load', 'mlife-theme' ); ?>
			</div>
			<?php 
				the_posts_navigation();	// keep the pagination for google and for the loadmore.js to find the next page.
			?>
		</div>
	<?php
	}
}
endif;



if ( ! function_exists( 'mlife_theme_term_customisation' ) ) :
/**
 * returns classes for customisation terms (taxonomy)
 */
function mlife_theme_term_customisation($term_id = false){
	if($term_id == false){
		return;
	}else{

		// TODO: remove this shit
		return  'colour term_customisation term_customisation--colour term_customisation--' . get_term($term_id)->name;
		/**
		 * Custom classes from customisation on the term done with ACF / in BO Category or Tags -. one of the Term -> None, Icon or colour, then conditionnal to appear 
		 */
		$classes = "";
		if(get_field('icon_colour','term_'.$term_id) != null && get_field('icon_colour','term_'.$term_id) != 'none'){ // if the style is setup and is different from none
			$type_customisation = get_field('icon_colour','term_'.$term_id);
			if($type_customisation == 'icon'){// if the chose style is icon
				if(get_field('icon','term_'.$term_id) != null){
					$classes = 'icon term_customisation--'.get_field('icon','term_'.$term_id); // build the classes to add
				}
			}
			if($type_customisation == 'colour'){// if the chose style is colour
				if(get_field('colour','term_'.$term_id) != null){
					$classes = 'colour term_customisation--'.get_field('colour','term_'.$term_id);// build the classes to add
				}
			}
			
			$classes = 'term_customisation term_customisation--' . $classes; // adding the classs to the array of classes to for the LI 
		}
		return $classes;
	}
}
endif;