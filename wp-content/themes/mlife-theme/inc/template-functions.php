<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package mlife-theme
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function mlife_theme_body_classes( $classes ) {

//   It check what pages it is and depending of that changes the colour of the header injecting class
    $current_category = get_category( get_query_var( 'cat' ) );
    // check if it is on category page, if true means yes, if not return null
    if (($current_category instanceof WP_Term)) {
        // Checking if It has colour
            if(get_field('colour','term_'. $current_category->term_id) != null) {
                $classes[] = 'header--'. get_field('colour','term_'. $current_category->term_id);
            }
        }

    // Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'mlife_theme_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function mlife_theme_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'mlife_theme_pingback_header' );


function mlife_theme_custom_setup() {
	
	/**** adding size for the redesign to build history of images ****/
	add_image_size( 'rectangle_grid', 400 , 360 , true );
	
}
add_action( 'after_setup_theme', 'mlife_theme_custom_setup' );



// "allow" HTML in the widget title
function html_widget_title_replace($html_widget_title) {

	$html_widget_title_tagopen = '['; //Our HTML opening tag replacement
	$html_widget_title_tagclose = ']'; //Our HTML closing tag replacement
	$html_widget_title_quote = '**'; //Our HTML opening tag replacement

	$html_widget_title = str_replace($html_widget_title_tagopen, '<', $html_widget_title);
	$html_widget_title = str_replace($html_widget_title_tagclose, '>', $html_widget_title);
	$html_widget_title = str_replace($html_widget_title_quote, '"', $html_widget_title);

	return $html_widget_title;
}
add_filter( 'widget_title', 'html_widget_title_replace' ); // allow html tags in widget title, ex : [i class=**fa fa-instagram**][/i]


/* Storing views of different time periods as meta keys */
add_action( 'wpp_post_update_views', 'custom_wpp_update_postviews' );

function custom_wpp_update_postviews($postid) {
	// Accuracy:
	//   10  = 1 in 10 visits will update view count. (Recommended for high traffic sites.)
	//   30 = 30% of visits. (Medium traffic websites)
	//   100 = Every visit. Creates many db write operations every request.

	$accuracy = 20;

	if ( function_exists('wpp_get_views') && (mt_rand(0,100) < $accuracy) ) {
		update_post_meta( $postid, 'views_total',   wpp_get_views( $postid ) );
	}

}


/** Used for deleting wordpress URL from a given url it will return a relative URL like /m-life/tutorials/ **/
function truncate_url($url){
	$pos = strpos($url, '/', strpos($url, '/')+2);
	return substr($url,$pos);
}
/** Used for deleting wordpress URL from a given url it will return an absolute URL like HYBRIS_URL/m-life/tutorials/ **/
function hybris_url($url){
	$pos = strpos($url, '/', strpos($url, '/')+2);
	return HYBRIS_URL.substr($url,$pos);
}


/**
 * Check if domain is from BU MCH, is used for displaying different assets
 * @return bool
 */
function buIsMCH()
{

    if (preg_match('(mch|.ch|-ch)', HYBRIS_URL)) {
        return true;
    }

    return false;
}

function add_role_to_body_admin($admin_body_classes) {// Add role class to body in the admin, used to hide some featured of left hand side menu
	
	global $current_user;
	$user_role_theme = $current_user->roles;
	$admin_body_classes .= ' role-'. $user_role_theme[0].' ';
	return $admin_body_classes;
}
add_filter('admin_body_class', 'add_role_to_body_admin');
