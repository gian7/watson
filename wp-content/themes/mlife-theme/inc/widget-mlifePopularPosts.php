<?php
/**
 * Functions which enhance the theme by creating more widgets into WordPress
The fields are added with advanced custom fields
 *
 * @package mlife_theme
 */
/**
 * Adds slickSlider widget.
 */
class mlifePopularPosts extends WP_Widget {
    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
            'mlifePopularPosts', // Base ID
            esc_html__( 'Mlife Popular Posts', 'text_domain' ), // Name
            array( 'description' => esc_html__( 'Popular Posts Widget to insert in the sidebar', 'text_domain' ), ) // Args
        );

    }
    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
        echo $args['before_widget'];

        if(get_field('title','widget_'.$args['widget_id'])){
            echo $args['before_title'].get_field('title','widget_'.$args['widget_id']).$args['after_title'];
        }


        $number_of_posts = 4; // get the number of post or 4 if not set
        if(get_field('number_of_posts','widget_'.$args['widget_id'])){
            $number_of_posts = get_field('number_of_posts','widget_'.$args['widget_id']);
        }


        $time_range = 'last30days'; // get the time range or last 30 days if not set
        if(get_field('time_range','widget_'.$args['widget_id'])){
            $time_range = get_field('time_range','widget_'.$args['widget_id']);
        }

        $WPP_query_args = array(
            'range' => $time_range,
            'limit' => $number_of_posts,
            'post_type' => 'post'
        ); // wpp query
        $WPP_query = new WPP_query( $WPP_query_args );
        $WPP_posts = $WPP_query->get_posts();
        $WPP_posts_id = array();
        // In case does not return anything, otherwise it will show first 10 posts
        if (empty($WPP_posts)) {
            $WPP_query_args = array(
                'limit' => $number_of_posts
            ); // wpp query
            $WP_query = new WP_Query( $WPP_query_args);
            $WPP_posts = $WP_query->get_posts();
        }


        foreach($WPP_posts as $WPP_post){
            $WPP_posts_id[] = $WPP_post->id;
        }


        $the_query_args = array(
            'post_type' => 'post',
            'post__in' => $WPP_posts_id,
            'orderby' => 'post__in'
        ); // post object that can be used with template parts
        $the_query = new WP_Query( $the_query_args );


        if ( $the_query->have_posts() ) {
            echo '<div class="mlifePopularPosts-container">';
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                get_template_part( 'template-parts/content', 'popular' );
            }
            wp_reset_postdata();
            echo '</div>';
        }

        echo $args['after_widget'];
    }
    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {

    }
    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {

    }

} // class mlifePopularPosts
// register mlifePopularPosts widget
function register_mlifePopularPosts() {
    register_widget( 'mlifePopularPosts' );
}
add_action( 'widgets_init', 'register_mlifePopularPosts' );

