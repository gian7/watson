<?php
/*
	
This is the PHP file that allows Wordpress to import the header and footer from Hybris and then display it either fully or a light version.
It runs every 7 days.
You can force it in the BO : Settings -> Hybris Preview

*/ 	


/*********** Add the Options in the options table and in the BO ***********/ 


class HybrisPreviewSettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Settings Admin', 
            'Hybris Preview', 
            'manage_options', 
            'hybris-preview', 
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'hybris_preview' );
        ?>
        <div class="wrap">
            <h1>Hybris Preview Settings</h1>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'hybris_settings_option_group' );
                do_settings_sections( 'hybris-preview' );
                submit_button();
            ?>
            </form>
            <p>The header and footer of Hybris is updated every 7 Days but you can force the update with this button : </p>
            <button id="update_hybris" class="button-primary">Force Updating</button><span id="update_hybris_result"><span class="dashicons dashicons-image-rotate"></span></span>
            <style>
	            #update_hybris_result{
		            display: none;
	            }
	            #update_hybris_result .dashicons{
		          margin:5px;
		        }
	        	#update_hybris_result .dashicons-image-rotate{
		          margin:5px;
		          -webkit-animation: rotating 2s linear infinite;
				  -moz-animation: rotating 2s linear infinite;
				  -ms-animation: rotating 2s linear infinite;
				  -o-animation: rotating 2s linear infinite;
				  animation: rotating 2s linear infinite;

	        	}
	        	@-webkit-keyframes rotating /* Safari and Chrome */ {
				  from {
				    -webkit-transform: rotate(360deg);
				    -o-transform: rotate(360deg);
				    transform: rotate(360deg); 
				  }
				  to {
				    -webkit-transform: rotate(0deg);
				    -o-transform: rotate(0deg);
				    transform: rotate(0deg);
				  }
				}
				@keyframes rotating {
				  from {
				    -ms-transform: rotate(360deg);
				    -moz-transform: rotate(360deg);
				    -webkit-transform: rotate(360deg);
				    -o-transform: rotate(360deg);
				    transform: rotate(360deg);
				  }
				  to {
				    -ms-transform: rotate(0deg);
				    -moz-transform: rotate(0deg);
				    -webkit-transform: rotate(0deg);
				    -o-transform: rotate(0deg);
				    transform: rotate(0deg);
				  }
				}
	        </style>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'hybris_settings_option_group', // Option group
            'hybris_preview', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            'Settings', // Title
            array( $this, 'print_section_info' ), // Callback
            'hybris-preview' // Page
        );        
        
        add_settings_field(
        'drop_down_preview', 
        'Select Preview', 
        array( $this,'setting_dropdown_fn'), 
        'hybris-preview', // Page
            'setting_section_id' // Section 
            );
    }
    
    // DROP-DOWN-BOX - Name: hybris_preview[preview]
	public function  setting_dropdown_fn() {
		$options = $this->options['preview'];
		
		$items = array(
			"light" => "Light", 
			"full" => "Full"
		);
		echo "<select id='drop_down_preview' name='hybris_preview[preview]'>";
		foreach($items as $key => $item) {
			$selected = ($options==$key) ? 'selected="selected"' : '';
			echo "<option value='$key' $selected>$item</option>";
		}
		echo "</select>";
	}

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        
        
        if( isset( $input['preview'] ) )
            $new_input['preview'] = sanitize_text_field( $input['preview'] );

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Choose below, between full or light preview of Hybris, knowing that full preview, is not responsive';
    }
}

if( is_admin() ){ // create new instance only on admin pages
	$my_settings_page = new HybrisPreviewSettingsPage();
}
    
add_action( 'admin_footer', 'hybris_preview_javascript' ); // Write our JS below here 


/** 
 * Print the JS for the  AJAX for the force update button 
 */
function hybris_preview_javascript() { ?>
	<script type="text/javascript" >
	jQuery(document).ready(function($) {
		
		jQuery("#update_hybris").click(function(){
				var data = {
				'action': 'update_hybris_header_footer',/* Do the action of updating the header and footer */
			};
			jQuery("#update_hybris_result").css('display','inline');
			jQuery.post(ajaxurl, data, function(response) {
				if(response){
					jQuery("#update_hybris_result").html('<span class="dashicons dashicons-yes"></span>Update Done');
				}
			});
		});		
		
		
	});
	</script> <?php
}

/*
	Attach the update function to the wp_ajax action
*/
add_action( 'wp_ajax_update_hybris_header_footer', 'update_hybris_header_footer_function' );




/*********** Cron Job to update Hybris Header and Footer on the option Table ***********/ 	




/*
	Attach the update function to an action	
*/
add_action( 'update_hybris_header_footer_action', 'update_hybris_header_footer_function' );


/*
	update function	
*/
function update_hybris_header_footer_function() {
	
	/* include the simplehtmldom utility */
	include(get_template_directory().'/vendor/simplehtmldom/simple_html_dom.php');
	
	
	/* get dom from live M-life / WP_FRONT_URL defined in wp-config (root) */
	$html = file_get_html(WP_FRONT_URL);
	
	/* extract the m-life part */
	$pageContainer = $html->getElementById("pageContainer");
    // if null, could be MCH site
    if (is_null($pageContainer)) {
        $pageContainer = $html->getElementById("page");

    }
	$rowpage = $pageContainer->first_child();
	
	/* empty the m-life part */
	$rowpage->innertext = "";
	
	/* extract the whole dom */
	$emptypage_wrongurls = $html->innertext;
	
	/* extract the head part (for light version) */
	$head_e = $html->getElementByTagName("head"); 
	$head_wrongurls = $head_e->innertext;
	
	
		/* search replace path from relative to absolute */
	$search = array('src="//','src="/','href="/','src=" /','href=" /');
	$replace = array('src="https://','src="'.HYBRIS_URL,'href="'.HYBRIS_URL,'src="'.HYBRIS_URL,'href="'.HYBRIS_URL);
	
	
	
	
	$emptypage = str_replace ( $search , $replace , $emptypage_wrongurls);
	$head = str_replace ( $search , $replace , $head_wrongurls);
	
	
	/* search remove meta tags and title because it breaks Hybris integration */
    $with       = '';
	$head    = preg_replace("/<meta[^>]*>/", $with, $head);
	$head    = preg_replace('/<link rel="(.*)icon(.*[^=]*)"[^>]*>/', $with, $head);
	$head    = preg_replace('/<title>[^>]*>/', $with, $head);
	
	
	
	/* split the dom before m-life and after m-life */
	$page_array = explode( '<div class="row content-page"></div>', $emptypage);
	
	
	/* building the light version */
	$header_light = '<!DOCTYPE html>
<html class="no-js">
	<head>
		'.$head.'
	</head>
	<body class="home blog custom-background fl-builder custom-background-image group-blog hfeed role-">
		<div id="sticky-menu">
		</div>';
					
	$footer_light='				
	</body>
</html>';			
	
	update_option("header_hybris",$page_array[0].'<div class="row content-page">');/* before m-life */
	update_option("footer_hybris",'</div>'.$page_array[1]);/* after m-life */
	update_option("header_hybris_light",$header_light);
	update_option("footer_hybris_light",$footer_light);
	
	return true;
}


// Add function to register event to WordPress init
add_action( 'init', 'update_hybris_header_footer_action_event');

// Function which will register the event
function update_hybris_header_footer_action_event() {
	// Make sure this event hasn't been scheduled
	if( !wp_next_scheduled( 'update_hybris_header_footer_action' ) ) {
		// Schedule the event
		wp_schedule_event( time(), '7_days', 'update_hybris_header_footer_action' );
	}
}

/* adding a 7_days interval */
add_filter( 'cron_schedules', 'add_cron_interval' );
 
function add_cron_interval( $schedules ) {
    $schedules['7_days'] = array(
        'interval' => 10080,
        'display'  => esc_html__( 'Every 7 days' ),
    );
 
    return $schedules;
}
