module.exports = function(grunt){
	
	grunt.initConfig({
		
		pkg: grunt.file.readJSON('package.json'),
		
		
		/******* Sass Task *******/
		sass:{
			dev:{
				options:{
					style: 'expanded',	
				},
				files: {
					'compiled/style-human.css':'sass/style.scss'
				}
			},
			dist:{
				options: {
					style: 'compressed'	
				},
				files: {
					'compiled/style.css':'sass/style.scss'
				}
			}
			
			

		} ,
		
		/******* AutoPrefixer Task *******/
		autoprefixer:{
			options:{
				browser: ['last 2 versions']
			},
			mutiple_files:{
				expand: 'true',
				flatten: 'true',
				src: 'compiled/*.css',
				dest: ''
			}
		} ,
		
		/******* Watch Task *******/
		watch:{
			css:{
				files: '**/*.scss',
				tasks: ['sass','autoprefixer']
			}
		} ,
		
		
		// Pot file generation task
		makepot: {
	        target: {
	            options: {   
		            cwd: './',                          // Directory of files to internationalize.                   
	                domainPath: '/languages',         // Where to save the POT file.
	                exclude: ['.git','acf-json','compiled','fonts','images','node_modules','sass'],
	                                   // List of files or directories to include.
	                mainFile: 'style.css',            // Main project file.
	                potComments: '',                  // The copyright at the beginning of the POT file.
	                potFilename: 'mlife-theme.pot',                  // Name of the POT file.
	                potHeaders: {
	                    poedit: true,                 // Includes common Poedit headers.
	                    'x-poedit-keywordslist': true // Include a list of all possible gettext functions.
	                },                                // Headers to add to the generated POT file.
	               
	                type: 'wp-theme',                // Type of project (wp-plugin or wp-theme).
	                updateTimestamp: true,            // Whether the POT-Creation-Date should be updated without other changes.
	                updatePoFiles: true              // Whether to update PO files in the same directory as the POT file.
	            }
	        }
	    }
		
		
	});
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.registerTask('default',['watch']);
	
	grunt.loadNpmTasks( 'grunt-wp-i18n' );
}