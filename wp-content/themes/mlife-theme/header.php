<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mlife-theme
 */
 
	/*
		Show either the full or light Hybris Header / The option is set up in BO Settings-> Hybris Preview / Generated in inc/hybris_header_footer.php
	*/
	if(get_option('hybris_preview',false)){// if the setup was done
		$hybris_preview =  get_option('hybris_preview');
		if($hybris_preview["preview"]=="full"){// if the setup is Full
			echo get_option('header_hybris');
		}else{
			echo get_option('header_hybris_light');
		}
	}else{
		echo get_option('header_hybris_light');
	}

?>

<main id="contentHubSectionId" role="main"  >
	<div <?php body_class("wordpress_body"); ?>>
		<?php wp_head(); ?>
	
	
<div id="page" class="site <?php if (buIsMCH()) : ?>multilingual<?php endif; ?>">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'mlife-theme' ); ?></a>

	<header id="masthead" class="site-header">
		
				
		<div class="site-branding">
			
			<?php 
				/*
				 *if the sidebar top-left is not empty show it / edit in BO Appearance->Widgets / Normally contains the menu with home icon not shown on home page
				*/
				if ( is_active_sidebar( 'top-left' ) ) : ?>
				<div id="top-left-sidebar" class="sidebar-header" role="complementary">
					<?php dynamic_sidebar( 'top-left' ); ?>
				</div>
			<?php endif; ?>
			<?php 
				/*
				 *if the sidebar top-right is not empty show it / edit in BO Appearance->Widgets / Normally contains the menu with feed icon and newsletter icon
				*/
				if ( is_active_sidebar( 'top-right' ) ) : ?>
				<div id="top-right-sidebar" class="sidebar-header" role="complementary">
					<?php dynamic_sidebar( 'top-right' ); ?>
				</div>
			<?php endif; ?>
		

			
			
			
			<?php
				
			$title_tag = "p";
			/*
			 * if we are on the home page then m-life logo is H1 otherwise it is in p tag
			*/
			if ( is_front_page() && is_home() ){
				$title_tag = "h1";	
			}
			
			/*
			 *if the logo is defined show it, otherwise show the name of the blog (can be changed in BO Settings->General)
			 *the logo is uploaded on the Appearance->Customize page
			*/
			if(get_custom_logo()){
				?>
				<<?php echo $title_tag ?> class="site-title">
					<?php the_custom_logo(); ?>
				</<?php echo $title_tag ?>>
				<?php
			}else{
				
				?>
				<<?php echo $title_tag ?> class="site-title">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<?php bloginfo( 'name' ); ?>
					</a>
				</<?php echo $title_tag ?>>
				<?php
			}
				
			
			$mlife_theme_description = get_bloginfo( 'description', 'display' );
			if ( $mlife_theme_description || is_customize_preview() ) :
				?>
				<p class="site-description">
                    <?php if (buIsMCH() === false) : // MCH not show description ?>
                        <?php echo $mlife_theme_description; /* WPCS: xss ok. */ ?>
                    <?php endif; ?>
                </p>
			<?php endif; ?>
		</div><!-- .site-branding -->
		
		<nav id="site-navigation" class="main-navigation">
			<a class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><span class="mlifeicon-menu"></span></a>
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu',
				'walker' => new Taxonomy_Walker(),
				
			) );
			?>
			<div class="clear"></div>
		</nav><!-- #site-navigation -->
		
		
		<?php 
			/*
			 * if the sidebar search is not empty show it / edit in BO Appearance->Widgets / Normally contains the search form
			*/
			if ( is_active_sidebar( 'search-bar' ) ) : ?>
			<div id="mlife-search-bar" class="search-bar-header" role="complementary">
				<?php dynamic_sidebar( 'search-bar' ); ?>
			</div>
		<?php endif; ?>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
