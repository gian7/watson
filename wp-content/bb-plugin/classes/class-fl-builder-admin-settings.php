<?php

/**
 * Handles logic for the admin settings page. 
 *
 * @since 1.0
 */
final class FLBuilderAdminSettings {
	
	/**
	 * Holds any errors that may arise from
	 * saving admin settings.
	 *
	 * @since 1.0
	 * @var array $errors
	 */
	static public $errors = array();
	
	/** 
	 * Adds the admin menu and enqueues CSS/JS if we are on
	 * the builder admin settings page.
	 *
	 * @since 1.0
	 * @return void
	 */
	static public function init()
	{
		add_action( 'admin_menu', 'FLBuilderAdminSettings::menu' );
			
		if ( isset( $_REQUEST['page'] ) && 'fl-builder-settings' == $_REQUEST['page'] ) {
			add_action( 'admin_enqueue_scripts', 'FLBuilderAdminSettings::styles_scripts' );
			self::save();
		}
	}
	
	/** 
	 * Enqueues the needed CSS/JS for the builder's admin settings page.
	 *
	 * @since 1.0
	 * @return void
	 */
	static public function styles_scripts()
	{
		// Styles
		wp_enqueue_style( 'fl-builder-admin-settings', FL_BUILDER_URL . 'css/fl-builder-admin-settings.css', array(), FL_BUILDER_VERSION );

		// Scripts
		wp_enqueue_script( 'fl-builder-admin-settings', FL_BUILDER_URL . 'js/fl-builder-admin-settings.js', array(), FL_BUILDER_VERSION );
		
		// Media Uploader
		wp_enqueue_media();
	}
	
	/** 
	 * Renders the admin settings menu.
	 *
	 * @since 1.0
	 * @return void
	 */
	static public function menu() 
	{
		if ( current_user_can( 'delete_users' ) ) {
			
			$title = FLBuilderModel::get_branding();
			$cap   = 'delete_users';
			$slug  = 'fl-builder-settings';
			$func  = 'FLBuilderAdminSettings::render';
			
			add_submenu_page( 'options-general.php', $title, $title, $cap, $slug, $func );
		}
	}
	
	/** 
	 * Renders the admin settings.
	 *
	 * @since 1.0
	 * @return void
	 */
	static public function render()
	{
		include FL_BUILDER_DIR . 'includes/admin-settings-js-config.php';
		include FL_BUILDER_DIR . 'includes/admin-settings.php';
	}
	
	/** 
	 * Renders the page class for network installs and single site installs.
	 *
	 * @since 1.0
	 * @return void
	 */
	static public function render_page_class()
	{
		if ( self::multisite_support() ) {
			echo 'fl-settings-network-admin';
		}
		else {
			echo 'fl-settings-single-install';
		}
	}
	
	/** 
	 * Renders the admin settings page heading.
	 *
	 * @since 1.0
	 * @return void
	 */
	static public function render_page_heading()
	{
		$icon = FLBuilderModel::get_branding_icon();
		$name = FLBuilderModel::get_branding();
		
		if ( ! empty( $icon ) ) {
			echo '<img src="' . $icon . '" />';
		}
		
		echo '<span>' . sprintf( _x( '%s Settings', '%s stands for custom branded "Page Builder" name.', 'fl-builder' ), FLBuilderModel::get_branding() ) . '</span>';
	}
	
	/** 
	 * Renders the update message.
	 *
	 * @since 1.0
	 * @return void
	 */	 
	static public function render_update_message()
	{
		if ( ! empty( self::$errors ) ) {
			foreach ( self::$errors as $message ) {
				echo '<div class="error"><p>' . $message . '</p></div>';
			}
		}
		else if( ! empty( $_POST ) && ! isset( $_POST['email'] ) ) {
			echo '<div class="updated"><p>' . __( 'Settings updated!', 'fl-builder' ) . '</p></div>';
		}
	}
	
	/** 
	 * Renders the nav items for the admin settings menu.
	 *
	 * @since 1.0
	 * @return void
	 */	  
	static public function render_nav_items()
	{
		$item_data = array(
			'welcome' => array(
				'title' => __( 'Welcome', 'fl-builder' ),
				'show'	=> FLBuilderModel::get_branding() == __( 'Page Builder', 'fl-builder' ) && ( is_network_admin() || ! self::multisite_support() )
			),
			'license' => array(
				'title' => __( 'License', 'fl-builder' ),
				'show'	=> FL_BUILDER_LITE !== true && ( is_network_admin() || ! self::multisite_support() )
			),
			'upgrade' => array(
				'title' => __( 'Upgrade', 'fl-builder' ),
				'show'	=> FL_BUILDER_LITE === true
			),
			'modules' => array(
				'title' => __( 'Modules', 'fl-builder' ),
				'show'	=> true
			),
			'templates' => array(
				'title' => __( 'Templates', 'fl-builder' ),
				'show'	=> FL_BUILDER_LITE !== true
			),
			'post-types' => array(
				'title' => __( 'Post Types', 'fl-builder' ),
				'show'	=> true
			),
			'icons' => 