<?php
// by Edd Dumbill (C) 1999-2002
// <edd@usefulinc.com>
// $Id: xmlrpc.inc,v 1.174 2009/03/16 19:36:38 ggiunta Exp $

// Copyright (c) 1999,2000,2002 Edd Dumbill.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//
//    * Redistributions in binary form must reproduce the above
//      copyright notice, this list of conditions and the following
//      disclaimer in the documentation and/or other materials provided
//      with the distribution.
//
//    * Neither the name of the "XML-RPC for PHP" nor the names of its
//      contributors may be used to endorse or promote products derived
//      from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.

$GLOBALS['xmlrpcName'] = 'Infusionsoft PHP iSDK 1.29.x ' . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'];
$GLOBALS['xmlrpcVersion'] = '3.0.0.beta';

if (!function_exists('xml_parser_create')) {
    // For PHP 4 onward, XML functionality is always compiled-in on windows:
    // no more need to dl-open it. It might have been compiled out on *nix...
    if (strtoupper(substr(PHP_OS, 0, 3) != 'WIN')) {
        dl('xml.so');
    }
}

// G. Giunta 2005/01/29: declare global these variables,
// so that xmlrpc.inc will work even if included from within a function
// Milosch: 2005/08/07 - explicitly request these via $GLOBALS where used.
$GLOBALS['xmlrpcI4'] = 'i4';
$GLOBALS['xmlrpcInt'] = 'int';
$GLOBALS['xmlrpcBoolean'] = 'boolean';
$GLOBALS['xmlrpcDouble'] = 'double';
$GLOBALS['xmlrpcString'] = 'string';
$GLOBALS['xmlrpcDateTime'] = 'dateTime.iso8601';
$GLOBALS['xmlrpcBase64'] = 'base64';
$GLOBALS['xmlrpcArray'] = 'array';
$GLOBALS['xmlrpcStruct'] = 'struct';
$GLOBALS['xmlrpcValue'] = 'undefined';

$GLOBALS['xmlrpcTypes'] = array(
    $GLOBALS['xmlrpcI4'] => 1,
    $GLOBALS['xmlrpcInt'] => 1,
    $GLOBALS['xmlrpcBoolean'] => 1,
    $GLOBALS['xmlrpcString'] => 1,
    $GLOBALS['xmlrpcDouble'] => 1,
    $GLOBALS['xmlrpcDateTime'] => 1,
    $GLOBALS['xmlrpcBase64'] => 1,
    $GLOBALS['xmlrpcArray'] => 2,
    $GLOBALS['xmlrpcStruct'] => 3
);

$GLOBALS['xmlrpc_valid_parents'] = array(
    'VALUE' => array('MEMBER', 'DATA', 'PARAM', 'FAULT'),
    'BOOLEAN' => array('VALUE'),
    'I4' => array('VALUE'),
    'INT' => array('VALUE'),
    'STRING' => array('VALUE'),
    'DOUBLE' => array('VALUE'),
    'DATETIME.ISO8601' => array('VALUE'),
    'BASE64' => array('VALUE'),
    'MEMBER' => array('STRUCT'),
    'NAME' => array('MEMBER'),
    'DATA' => array('ARRAY'),
    'ARRAY' => array('VALUE'),
    'STRUCT' => array('VALUE'),
    'PARAM' => array('PARAMS'),
    'METHODNAME' => array('METHODCALL'),
    'PARAMS' => array('METHODCALL', 'METHODRESPONSE'),
    'FAULT' => array('METHODRESPONSE'),
    'NIL' => array('VALUE'), // only used when extension activated
    'EX:NIL' => array('VALUE') // only used when extension activated
);

// define extra types for supporting NULL (useful for json or <NIL/>)
$GLOBALS['xmlrpcNull'] = 'null';
$GLOBALS['xmlrpcTypes']['null'] = 1;

// Not in use anymore since 2.0. Shall we remove it?
/// @deprecated
$GLOBA