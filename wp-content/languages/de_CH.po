# <!=Copyright (C) 2018 Underscores.me
# This file is distributed under the GNU General Public License v2 or later.=!>
msgid ""
msgstr ""
"Project-Id-Version: mlife-theme 1.0.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/theme/style\n"
"POT-Creation-Date: 2018-07-02 09:50+0100\n"
"PO-Revision-Date: 2018-07-10 12:55+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: de_CH\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;"
"_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;"
"esc_html_x:1,2c\n"
"X-Poedit-Basepath: ..\n"
"X-Textdomain-Support: yes\n"
"X-Generator: Poedit 2.0.8\n"
"X-Poedit-SearchPath-0: .\n"

#: 404.php:19
msgid "Oops!"
msgstr "UPS!"

#: 404.php:23
msgid "It looks like this page doesn't exist anymore"
msgstr "Diese Seite gibt es anscheinend nicht mehr."

#: 404.php:24
msgid "Are you looking for something specific?"
msgstr "Suchst du nach etwas Bestimmten?"

#: 404.php:26
msgid "Try it with our search function"
msgstr "Probier‘ es doch einfach mit unserer Suchfunktion:"

#: 404.php:64
msgctxt "mlife-theme"
msgid "Discover the Trends ..."
msgstr "Entdecke die neusten Trends…"

#: bb-components/feature-products/feature-products.php:8
msgid "Feature Product"
msgstr ""

#: bb-components/feature-products/feature-products.php:9
msgid "Insert a Featured Product in you Layout"
msgstr ""

#: bb-components/feature-products/feature-products.php:10
#: bb-components/product-carousel/product-carousel.php:10
msgid "Hybris MLife"
msgstr ""

#: bb-components/feature-products/feature-products.php:46
#: bb-components/product-carousel/product-carousel.php:26
msgid "General"
msgstr ""

#: bb-components/feature-products/feature-products.php:49
msgid "Product"
msgstr ""

#: bb-components/feature-products/feature-products.php:53
msgid "Article ID"
msgstr ""

#: bb-components/feature-products/feature-products.php:64
msgid "Hybris Category for BackUp"
msgstr ""

#: bb-components/feature-products/feature-products.php:72
msgid "Product Image"
msgstr ""

#: bb-components/feature-products/feature-products.php:78
msgid "Layout"
msgstr ""

#: bb-components/feature-products/feature-products.php:83
msgid "1 Column"
msgstr ""

#: bb-components/feature-products/feature-products.php:84
msgid "2 Columns"
msgstr ""

#: bb-components/feature-products/feature-products.php:85
msgid "1 Columns + Text"
msgstr ""

#: bb-components/feature-products/feature-products.php:86
msgid "2 Columns + Text"
msgstr ""

#: bb-components/feature-products/feature-products.php:99
msgid "Title"
msgstr ""

#: bb-components/feature-products/feature-products.php:115
msgid "Background Image"
msgstr ""

#: bb-components/feature-products/feature-products.php:121
msgid "Border"
msgstr ""

#: bb-components/feature-products/feature-products.php:125
msgid "Yes"
msgstr ""

#: bb-components/feature-products/feature-products.php:126
msgid "No"
msgstr ""

#: bb-components/feature-products/includes/frontend.php:18
msgctxt "mlife-theme"
msgid "Go to the Shop"
msgstr "Zurück zum Shop"

#: bb-components/feature-products/includes/frontend.php:101
#: bb-components/feature-products/includes/frontend.php:104
msgctxt "mlife-theme"
msgid "Shop Now"
msgstr "Jetzt Kaufen"

#: bb-components/product-carousel/product-carousel.php:8
msgid "Product Carousel"
msgstr ""

#: bb-components/product-carousel/product-carousel.php:9
msgid "Insert a ProductCarousel in you Layout"
msgstr ""

#: bb-components/product-carousel/product-carousel.php:29
msgid "Carousel"
msgstr ""

#: bb-components/product-carousel/product-carousel.php:33
msgid "Carousel ID"
msgstr ""

#: comments.php:35
#, php-format
msgid "One thought on &ldquo;%1$s&rdquo;"
msgstr ""

#: comments.php:41
#, php-format
msgctxt "comments title"
msgid "%1$s thought on &ldquo;%2$s&rdquo;"
msgid_plural "%1$s thoughts on &ldquo;%2$s&rdquo;"
msgstr[0] ""
msgstr[1] ""

#: comments.php:66
msgid "Comments are closed."
msgstr ""

#: functions.php:49
msgid "Primary"
msgstr ""

#: functions.php:112
msgid "Sidebar"
msgstr ""

#: functions.php:114 functions.php:127 functions.php:136
msgid "Add widgets here."
msgstr ""

#: functions.php:125
msgid "Top Right"
msgstr ""

#: functions.php:134
msgid "Top Left"
msgstr ""

#: header.php:34
msgid "Skip to content"
msgstr ""

#: inc/customizer.php:35
msgid "Random Backup"
msgstr ""

#: inc/customizer.php:44
msgid "Image 1"
msgstr ""

#: inc/customizer.php:55
msgid "Image 2"
msgstr ""

#: inc/customizer.php:65
msgid "Image 3"
msgstr ""

#: inc/customizer.php:78
msgid "Which Taxonomy should I use to search for Backup Images ?"
msgstr ""

#: inc/customizer.php:80
msgid "Tags"
msgstr ""

#: inc/customizer.php:81
msgid "Categories"
msgstr ""

#: inc/hybris_header_footer.php:323
msgid "Every 7 days"
msgstr ""

#: inc/product_tiles_backup.php:11 inc/product_tiles_backup.php:12
msgid "Product Tiles Backup"
msgstr ""

#: inc/product_tiles_backup.php:29
msgctxt "taxonomy general name"
msgid "SKU Hybris Taxonomies"
msgstr ""

#: inc/product_tiles_backup.php:30
msgctxt "taxonomy singular name"
msgid "SKU Hybris Taxonomy"
msgstr ""

#: inc/product_tiles_backup.php:31
msgid "Search SKU Hybris Taxonomies"
msgstr ""

#: inc/product_tiles_backup.php:32
msgid "All SKU Hybris Taxonomies"
msgstr ""

#: inc/product_tiles_backup.php:33
msgid "Parent SKU Hybris Taxonomy"
msgstr ""

#: inc/product_tiles_backup.php:34
msgid "Parent SKU Hybris Taxonomy:"
msgstr ""

#: inc/product_tiles_backup.php:35
msgid "Edit SKU Hybris Taxonomy"
msgstr ""

#: inc/product_tiles_backup.php:36
msgid "Update SKU Hybris Taxonomy"
msgstr ""

#: inc/product_tiles_backup.php:37
msgid "Add New SKU Hybris Taxonomy"
msgstr ""

#: inc/product_tiles_backup.php:38
msgid "New SKU Hybris Taxonomy Name"
msgstr ""

#: inc/product_tiles_backup.php:39
msgid "SKU Hybris Taxonomy"
msgstr ""

#: inc/product_tiles_backup.php:54
msgctxt "taxonomy general name"
msgid "Hybris CAT Taxonomies"
msgstr ""

#: inc/product_tiles_backup.php:55
msgctxt "taxonomy singular name"
msgid "Hybris CAT Taxonomy"
msgstr ""

#: inc/product_tiles_backup.php:56
msgid "Search CAT Hybris Taxonomies"
msgstr ""

#: inc/product_tiles_backup.php:57
msgid "All CAT Hybris Taxonomies"
msgstr ""

#: inc/product_tiles_backup.php:58
msgid "Parent CAT Hybris Taxonomy"
msgstr ""

#: inc/product_tiles_backup.php:59
msgid "Parent CAT Hybris Taxonomy:"
msgstr ""

#: inc/product_tiles_backup.php:60
msgid "Edit CAT Hybris Taxonomy"
msgstr ""

#: inc/product_tiles_backup.php:61
msgid "Update CAT Hybris Taxonomy"
msgstr ""

#: inc/product_tiles_backup.php:62
msgid "Add New CAT Hybris Taxonomy"
msgstr ""

#: inc/product_tiles_backup.php:63
msgid "New CAT Hybris Taxonomy Name"
msgstr ""

#: inc/product_tiles_backup.php:64
msgid "CAT Hybris Taxonomy"
msgstr ""

#: inc/template-tags.php:29
#, php-format
msgctxt "post date"
msgid "Posted on %s"
msgstr ""

#: inc/template-tags.php:45
#, php-format
msgctxt "post author"
msgid "by %s"
msgstr ""

#: inc/template-tags.php:63
msgid ", "
msgstr ""

#: inc/template-tags.php:66
#, php-format
msgid "Posted in %1$s"
msgstr ""

#: inc/template-tags.php:70
msgctxt "list item separator"
msgid ", "
msgstr ""

#: inc/template-tags.php:73
#, php-format
msgid "Tagged %1$s"
msgstr ""

#: inc/template-tags.php:83
#, php-format
msgid "Leave a Comment<span class=\"screen-reader-text\"> on %s</span>"
msgstr ""

#: inc/template-tags.php:100 template-parts/content-page.php:37
#, php-format
msgid "Edit <span class=\"screen-reader-text\">%s</span>"
msgstr ""

#: inc/template-tags.php:365 inc/template-tags.php:437
msgctxt "mlife-theme"
msgid "Sort by :"
msgstr "Sortieren nach :"

#: inc/template-tags.php:371 inc/template-tags.php:380
#: inc/template-tags.php:383 inc/template-tags.php:389
#: inc/template-tags.php:443 inc/template-tags.php:452
#: inc/template-tags.php:455 inc/template-tags.php:461
msgctxt "mlife-theme"
msgid "Most Recent"
msgstr "Neueste"

#: inc/template-tags.php:374 inc/template-tags.php:390
#: inc/template-tags.php:446 inc/template-tags.php:462
msgctxt "mlife-theme"
msgid "Most Popular"
msgstr "Beliebteste"

#: inc/template-tags.php:377 inc/template-tags.php:391
#: inc/template-tags.php:449 inc/template-tags.php:463
msgctxt "mlife-theme"
msgid "Oldest"
msgstr "Älteste"

#: inc/template-tags.php:490
msgctxt "mlife-theme"
msgid "of"
msgstr "von"

#: inc/template-tags.php:496
msgctxt "mlife-theme"
msgid "Show More Articles"
msgstr "Mehr Artikel anzeigen"

#: inc/template-tags.php:500
msgctxt "mlife-theme"
msgid "No more to load"
msgstr "Keine weiteren Artikel"

#: inc/widget-mlifePopularPosts.php:18
msgid "Mlife Popular Posts"
msgstr ""

#: inc/widget-mlifePopularPosts.php:19
msgid "Popular Posts Widget to insert in the sidebar"
msgstr ""

#: node_modules/grunt-wp-i18n/test/fixtures/basic-theme/exclude/file.php:3
#: node_modules/grunt-wp-i18n/test/fixtures/plugin-include/plugin-include.php:6
msgid "Exclude"
msgstr ""

#: node_modules/grunt-wp-i18n/test/fixtures/plugin-include/include/file.php:2
msgid "Include"
msgstr ""

#: node_modules/grunt-wp-i18n/test/fixtures/text-domains/add-domain.php:2
#: node_modules/grunt-wp-i18n/test/fixtures/text-domains/update-all-domains.php:2
#: node_modules/grunt-wp-i18n/test/fixtures/text-domains/update-all-domains.php:3
#: node_modules/grunt-wp-i18n/test/fixtures/text-domains/update-all-domains.php:4
#: node_modules/grunt-wp-i18n/test/fixtures/text-domains/update-domains.php:2
#: node_modules/grunt-wp-i18n/test/fixtures/text-domains/update-domains.php:3
msgid "String"
msgstr ""

#: node_modules/grunt-wp-i18n/test/fixtures/text-domains/update-all-domains.php:6
#: node_modules/grunt-wp-i18n/test/fixtures/text-domains/update-all-domains.php:7
#: node_modules/grunt-wp-i18n/test/fixtures/text-domains/update-all-domains.php:8
msgctxt "a string"
msgid "String"
msgstr ""

#: node_modules/grunt-wp-i18n/test/fixtures/text-domains/update-all-domains.php:9
#: node_modules/grunt-wp-i18n/test/fixtures/text-domains/update-all-domains.php:10
#: node_modules/grunt-wp-i18n/test/fixtures/text-domains/update-all-domains.php:11
#, php-format
msgid "1 Star"
msgid_plural "%s Stars"
msgstr[0] ""
msgstr[1] ""

#: node_modules/grunt-wp-i18n/test/fixtures/translator-comments/translator-comments.php:7
msgid "A"
msgstr ""

#: node_modules/grunt-wp-i18n/test/fixtures/translator-comments/translator-comments.php:15
msgid "B"
msgstr ""

#: search.php:22
#, php-format
msgid "Search Results for: %s"
msgstr "Gefundene Ergebnisse für: %s"

#: search.php:28
msgctxt "mlife-theme"
msgid "Your search returned <strong>0</strong> results"
msgstr "Deine Suche ergab keine Ergebnisse"

#: search.php:30
msgctxt "mlife-theme"
msgid "Your search returned <strong>1</strong> result"
msgstr "Deine Suche ergab <strong>1</strong> Ergebnis"

#: search.php:32
#, php-format
msgid "Your search returned %s results"
msgstr "Deine Suche ergab %s Ergebnisse"

#: search.php:63
msgctxt "mlife-theme"
msgid "Unfortunately, your search doesn't match with any of our articles"
msgstr "Leider ergab deine Suche keine Ergebnisse."

#: search.php:98
msgctxt "mlife-theme"
msgid "Need Inspiration? You might like that ..."
msgstr "Inspiration gefällig? Das könnte Ihnen gefallen…"

#: searchform.php:13 searchform.php:14
msgctxt "label"
msgid "Search for:"
msgstr ""

#: searchform.php:14
msgctxt "placeholder"
msgid "Search The Blog"
msgstr "Blog durchsuchen"

#: single.php:57
msgctxt "mlife-theme"
msgid "Next"
msgstr "Nächster"

#: single.php:58
msgctxt "mlife-theme"
msgid "Previous"
msgstr "Vorheriger"

#: single.php:96 single.php:138
msgctxt "mlife-theme"
msgid "Share this Article:"
msgstr "Artikel teilen:"

#: template-parts/content-none.php:14
msgid "Nothing Found"
msgstr ""

#: template-parts/content-none.php:24
#, php-format
msgid ""
"Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#: template-parts/content-none.php:37
msgid ""
"Sorry, but nothing matched your search terms. Please try again with some "
"different keywords."
msgstr ""

#: template-parts/content-none.php:44
msgid ""
"It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps "
"searching can help."
msgstr ""

#: template-parts/content-page.php:24
msgid "Pages:"
msgstr ""

#: template-parts/content-popular.php:15 template-parts/content.php:15
msgid "Featured"
msgstr ""

#: template-parts/content-popular.php:40 template-parts/content-slider.php:25
#: template-parts/content.php:70
msgctxt "mlife-theme"
msgid "Read More"
msgstr "Weiterlesen"
