��          �   %   �      P  '   Q  -   y     �     �     �     �  #        (     C     \  5   t     �     �     �     �     �          5     J     i  M     ;   �  :   	     D     S  �  o     7      P     q     w     �  6   �     �     	     	     ,	  <   4	     q	     y	     �	     �	     �	     �	     �	     �	     �	  ?   
  2   H
  1   {
     �
  5   �
                                                                                             	                         
       Are you looking for something specific? It looks like this page doesn't exist anymore Oops! Search Results for: %s Try it with our search function Your search returned %s results mlife-themeDiscover the Trends ... mlife-themeGo to the Shop mlife-themeMost Popular mlife-themeMost Recent mlife-themeNeed Inspiration? You might like that ... mlife-themeNext mlife-themeNo more to load mlife-themeOldest mlife-themePrevious mlife-themeRead More mlife-themeShare this Article: mlife-themeShop Now mlife-themeShow More Articles mlife-themeSort by : mlife-themeUnfortunately, your search doesn't match with any of our articles mlife-themeYour search returned <strong>0</strong> results mlife-themeYour search returned <strong>1</strong> result mlife-themeof placeholderSearch The Blog Project-Id-Version: mlife-theme 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/support/theme/style
POT-Creation-Date: 2018-07-02 09:57+0100
PO-Revision-Date: 2018-07-10 12:45+0100
Last-Translator: 
Language-Team: 
Language: ro_RO
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Generator: Poedit 2.0.8
X-Poedit-SearchPath-0: .
 Cautati ceva in special? Pagina cautata nu a fost gasita. Oops! Résultats trouvés pour: %s Foloseste campul de CAUTARE: Cautarea ta a rezultat <strong>%s</strong> de articole Descoperiti noile trenduri Catre magazinul online Cele mai populare Noutati Cauti inspiratie? Arunca-ti un ochi peste articolele noastre Inainte Nici un alt articol disponibil Cele mai vechi Inapoi Citeste mai mult Distribuie acest articol: Cumpara acum Arata mai multe articole Sorteaza Din pacate, cautarea dumneavoastra nu a generat nici un articol Cautarea ta a rezultat <strong>0</strong> articole Cautarea ta a rezultat <strong>1</strong> articol din Cum poate M-life sa te inspire? Cauta si vei afla … 