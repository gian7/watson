<?php
/*
Plugin Name: MLife REST API Modifications
Description: Minor additions to the REST API endpoint.
Version: 1.0.0
*/

add_action('rest_api_init', 'hybris_register_expose_taxonomy_name');
function hybris_register_expose_taxonomy_name() {
  $taxonomies = array(
    'cat_taxonomy', 
    'sku_taxonomy',
  );
  foreach ($taxonomies as $taxonomy) {
  register_rest_field('post',
    $taxonomy,
    array(
      'get_callback' => 'hybris_expose_taxonomy_name',
      'update_callback' => null,
      'schema' => null,
      )
    );
  }
}
// Return the 'real' name of the defined taxonomies to the REST API
function hybris_expose_taxonomy_name($object, $field_name, $request) {
  $terms = get_the_terms($object['id'] , $field_name);
  $results = array();
  foreach ($terms as $term) {
    $results[] = $term->name;
  }
  return $results;
}
