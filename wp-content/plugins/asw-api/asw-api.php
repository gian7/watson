<?php
/*
Plugin Name: ASW API Extension
Description: Extension of the REST API 2.0
Version: 1.0.1
*/

/*
 * Show Advanced Custom Fields in JSON response
 */
function asw_wp_rest_api_acf() {
  register_api_field('post',
    'fields',
    array(
      'get_callback' => function($data, $field, $request, $type) {
        if (function_exists('get_fields')) {
          $acfields = get_fields($data['id']);
          // exclude specified fields
          unset($acfields['custom_styles']);
          unset($acfields['custom_javascript']);
          return $acfields;

        }
        return [];
      },
      'update_callback' => null,
      'schema' => null,
    )
  );
}
add_action( 'rest_api_init', 'asw_wp_rest_api_acf');
